
$("#addCar").click(function(){
    $("#showAddVeh").show();
    $("#showVehUsers").hide();
});

$("#cancel").click(function(){
    $("#showVehUsers").show();
    $("#showAddVeh").hide();
});


window.onload = function() {
    viewVehiculos();
};




function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function valida3(nname){

	if( (nname=="")||(nname.length<3)){
		return false;
	}
	else{
		return true;
	}
}

function validaDate(fecha){
	if ( isNaN(fecha)==false && fecha.length == 4){ 
		return true; 
	} 
	else { 
		return false; 
	}  
}

function validaSel(sel){
	if(sel.val()=="seleccionar"){
		return false;
	}
	else{
		return true;
	}
}

function registerVehiculo(){
	var flag= true;
	var data=[];
    var inputs= $("#addVeh input").each(function(){
    	data.push($(this).val());
        if($(this).attr("name")=="matricula5"){
            if(valida3($(this).val().trim())==false){
                flag=false;
                $(this).removeClass("noerror");
                $(this).addClass("error");
            }
            else{
                $(this).removeClass("error");
                $(this).addClass("noerror");
            }
        }
        else if($(this).attr("name")=="marca5"){
            if(valida3($(this).val().trim())==false){
                flag=false;
                $(this).removeClass("noerror");
                $(this).addClass("error");
            }
            else{
                $(this).removeClass("error");
                $(this).addClass("noerror");
            }
        }
        else if($(this).attr("name")=="año5"){
            $(this).removeClass("noerror");
            $(this).removeClass("error");
            if($(this).val()!=""){
                if(validaDate($(this).val().trim())==false){
                    flag=false;
                    $(this).removeClass("noerror");
                    $(this).addClass("error");
                }
                else{
                    $(this).removeClass("error");
                    $(this).addClass("noerror");
                }
            }
        }
    });
    var select= $("#addVeh select").each(function(){
    	data.push($(this).val());
        if($(this).attr("name")=="tipoVehiculo"){
            if(validaSel($(this))==false){
                flag=false;
                $(this).removeClass("noerror");
                $(this).addClass("error");
            }
            else{
                $(this).removeClass("error");
                $(this).addClass("noerror");
            }
        }
    });
    if(flag==true){;
    	data.push(getParameterByName('id'));
        var outPutData;
        var jsonData = JSON.stringify(data);
        $.ajax({
            type: "POST",
            async: false,
            url: "clases/controller.php", 
            data: "action=4&data="+jsonData,
            datatype: "json",
            success: function (dataSuccess) {
                outPutData=dataSuccess;
            },
            error: function (error){

            }
        });
		if(outPutData==false){
            $("#mens p").remove();
			$("#mens").append("<p class='errorText'>ESTA MATRICULA YA EXISTE </p>");
		}
        else{
            viewVehiculos();
            $("#showVehUsers").show();
            $("#showAddVeh").hide();
        }
	}
}

function viewVehiculos(){
    var rows;
    $.ajax({
            type: "POST",
            async: false,
            url: "clases/controller.php", 
            data: "action=5&index="+getParameterByName('id'),
            datatype: "json",

            success: function (dataSuccess) {
                rows=JSON.parse(dataSuccess);

            },
            error: function (error){

            }
    });
    $("#allVehicles div").remove();
    for(i=0;i<rows.length;i++){
        var div= "<div class='carDiv'><i class='fa fa-trash deleteVeh' onclick=\"deleteVehicle('"+rows[i].matricula_id+"')\" aria-hidden='true'></i><a href=historialAdmin.php?id="+rows[i].matricula_id+"><div class='enlaceVeh'>";
        if(rows[i].type=='turismo'){
             div+= "<i class='fa fa-car fa-4x arriva' aria-hidden='true'></i>";
        }

        else if(rows[i].type=='motocicleta'){
            div+= "<div class='vehImg arriva'><img src='img/motorcycle.svg'></div>";
        }

        else if(rows[i].type=='ciclomotor'){
            div+= "<i class='fa fa-motorcycle fa-4x arriva aria-hidden='true'></i>";
        }

        else if(rows[i].type=='quad'){
            div+= "<div class='vehImg arriva'><img src='img/quad.svg'></div>";
        }

        else if(rows[i].type=='furngoneta'){
          div+= "<div class='vehImg arriva'><img src='img/truck.svg'></div>";
        }

        else if(rows[i].type=='tresruedas'){
          div+= "<div class='vehImg arriva'><img src='img/trimoto.svg'></div>";
        }
         div+="<p><big>"+(rows[i].matricula_id).toUpperCase()+"</big></p>";
        if(rows[i].modelo!=""){
            div+="<p>"+(rows[i].modelo).toUpperCase()+"</p>";
        }
        else{
            div+="<p>"+(rows[i].marca).toUpperCase()+"</p>";
        }
         div+="</div></a></div>";
        $("#allVehicles").append(div);
    }
}

function deleteVehicle(id){
    var r = confirm("Estas seguro que deseas eliminar este vehiculo? (Se eliminara el historial completo de este)");
    if (r == true) {
        $.ajax({
                type: "POST",
                async: false,
                url: "clases/controller.php", 
                data: "action=15&id="+id,
                datatype: "json",

                success: function (dataSuccess) {
                    rows=JSON.parse(dataSuccess);

                },
                error: function (error){
                }
        });
        if(rows==true){
            location.reload(true);
        }
    }
};
