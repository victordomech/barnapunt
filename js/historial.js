$("#rPlus").click(function(){
    $("#showAddHist").show();
    $("#showHistorial").hide();
});

$("#cancel").click(function(){
    $("#showHistorial").show();
    $("#showAddHist").hide();
});

window.onload = function() {
    viewHistorial();
    searchVehicle();
};

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

function ued_encode(arr,current_index) 
{
  var query = ""
  if(typeof current_index=='undefined') current_index = '';

  if(typeof(arr) == 'object') {
    var params = new Array();
    for(key in arr) {
      var data = arr[key];
      var key_value = key;
      if(current_index) {
        key_value = current_index+"["+key+"]"
      }

      if(typeof(data) == 'object') {
        if(data.length) { //List
          for(var i=0;i<data.length; i++) {
            params.push(key_value+"[]="+ued_encode(data[i],key_value)); //:RECURSION:
          }
        } else { //Associative array
          params.push(ued_encode(data,key_value)); //:RECURSION:
        }
      } else { //String or Number
        params.push(key_value+"="+encodeURIComponent(data));
      }
    }
    query = params.join("&");
  } else {
    query = encodeURIComponent(arr);
  }

  return query;
}
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function valida3(nname){

	if( (nname=="")||(nname.length<3)){
		return false;
	}
	else{
		return true;
	}
}

function validaSel(sel){
	if(sel.val()=="seleccionar"){
		return false;
	}
	else{
		return true;
	}
}

function validaPrice(num){
    if(num == 0 || num.includes(",")==true){
        return false;
    }
    else{
        return true;
    }

}

function validaFile(file) { 
   var extensiones_permitidas = ".pdf"; 
   var mierror = ""; 
   var archivo= file.val();
   if (!archivo) { 
     return false;
   }
   else{ 
      //recupero la extensión de este nombre de archivo 
      extension = (archivo.substring(archivo.lastIndexOf("."))).toLowerCase(); 
      //alert (extension); 
      //compruebo si la extensión está entre las permitidas 
      permitida = false; 
        if (extensiones_permitidas == extension) { 
        return true;
        }
        else{
        	return false;
        } 
   } 
}

function registerVisit(){
	var flag= true;
	var data=[];
	data.push(getParameterByName('id'));
	var select= $("#addVisit select").each(function(){
    	data.push($(this).val());
        if($(this).attr("name")=="services"){
            if(validaSel($(this))==false){
                flag=false;
                $(this).removeClass("noerror");
                $(this).addClass("error");
            }
            else{
                $(this).removeClass("error");
                $(this).addClass("noerror");
            }
        }

    });
    var area= $("#addVisit textarea").each(function(){
    	data.push($(this).val());
        if($(this).attr("name")=="commentary3"){
            if(valida3($(this).val().trim())==false){
                flag=false;
                $(this).removeClass("noerror");
                $(this).addClass("error");
            }
            else{
                $(this).removeClass("error");
                $(this).addClass("noerror");
            }
        }

    });

    var inputs= $("#addVisit input").each(function(){
        if($(this).attr("name")=="factura"){
            if(validaFile($(this))==false){
                flag=false;
                $(this).removeClass("noerror");
                $(this).addClass("error");
            }
            else{
                $(this).removeClass("error");
                $(this).addClass("noerror");
            }
        }
        else if($(this).attr("name")=="abonado"){
            data.push($(this).val());
            if(validaPrice($(this).val().trim())==false){
                flag=false;
                $(this).removeClass("noerror");
                $(this).addClass("error");
            }
            else{
                $(this).removeClass("error");
                $(this).addClass("noerror");
            }
        }

    });

   if(flag==true){
        var outPutData;
        var file = $("#factura")[0].files[0].name;
        data.push(file);
        data[2]=ued_encode(data[2]);
        var jsonData = JSON.stringify(data);

        $.ajax({
            type: "POST",
            async: false,
            url: "clases/controller.php", 
            data: "action=6&data="+jsonData,
            datatype: "json",
            success: function (dataSuccess) {
                outPutData=dataSuccess;
            },
            error: function (error){

            }
        });
		if(outPutData==false){
            return false;
		}
        else{
            return true;
        }
	}
    else{
        return false;
    }
}

function viewHistorial(){
    var rows;
    $.ajax({
            type: "POST",
            async: false,
            url: "clases/controller.php", 
            data: "action=7&index="+getParameterByName('id'),
            datatype: "json",

            success: function (dataSuccess) {
                rows=JSON.parse(dataSuccess);
            },
            error: function (error){

            }
    });
    $("#showHistorial div").remove();
    for(i=0;i<rows.length;i++){
        var div= "<div class='list-group'><p class='list-group-item active'>"+(rows[i].servicio)+"<i class='fa fa-trash deleteHist' onclick='deleteVisit("+(rows[i].id)+")' aria-hidden='true' ></i><span class='right'>"+(rows[i].abonado)+" €</span></p><div class='list-group-item'>"+rows[i].dia+"</div>";
        div+="<div class='list-group-item'><p class='list-group-item-text'>"+rows[i].descripcion+"</p>";
        div+="</div><a class='list-group-item active' href='"+rows[i].factura+"' target='_blank'>Si quieres descargar la factura pincha aqui <span><i class='fa fa-file-pdf-o derecha' aria-hidden='true'></i></span></a></div>";
        $("#showHistorial").append(div);
    };
}

function searchVehicle(){
    $.ajax({
            type: "POST",
            async: false,
            url: "clases/controller.php", 
            data: "action=12",
            datatype: "json",

            success: function (dataSuccess) {
                rows=JSON.parse(dataSuccess);

            },
            error: function (error){
            }
    });
    var tr;
    for(i=0;i<rows.length;i++){
        $("#infoVeh").append("<p>&nbsp;&nbsp;"+rows[i].marca+" "
        +rows[i].modelo);
    };
}

function deleteVisit(id){
    var r = confirm("Estas seguro que deseas eliminar esta visita?");
    if (r == true) {
        $.ajax({
                type: "POST",
                async: false,
                url: "clases/controller.php", 
                data: "action=14&id="+id,
                datatype: "json",

                success: function (dataSuccess) {
                    rows=JSON.parse(dataSuccess);

                },
                error: function (error){
                }
        });
        sleep(1000);
        viewHistorial();
    }
};

