window.onload = function() {
    viewVehiculos();
};




function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


function viewVehiculos(){
    var rows;
    $.ajax({
            type: "POST",
            async: false,
            url: "clases/controller.php", 
            data: "action=9",
            datatype: "json",

            success: function (dataSuccess) {
                rows=JSON.parse(dataSuccess);
            },
            error: function (error){

            }
    });
    $("#allVehicles div").remove();
   for(i=0;i<rows.length;i++){
        var div= "<a href=historial.php?id="+rows[i].matricula_id+"><div class='carDiv'>";
        if(rows[i].type=='turismo'){
             div+= "<i class='fa fa-car fa-4x arriva' aria-hidden='true'></i>";
        }

        else if(rows[i].type=='motocicleta'){
            div+= "<div class='vehImg arriva'><img src='img/motorcycle.svg'></div>";
        }

        else if(rows[i].type=='ciclomotor'){
            div+= "<i class='fa fa-motorcycle fa-4x arriva aria-hidden='true'></i>";
        }

        else if(rows[i].type=='quad'){
            div+= "<div class='vehImg arriva'><img src='img/quad.svg'></div>";
        }

        else if(rows[i].type=='furngoneta'){
          div+= "<div class='vehImg arriva'><img src='img/truck.svg'></div>";
        }

        else if(rows[i].type=='tresruedas'){
          div+= "<div class='vehImg arriva'><img src='img/trimoto.svg'></div>";
        }

        div+="<p>"+(rows[i].marca).toUpperCase()+"</p></div></a>";
        $("#allVehicles").append(div);
    }
}