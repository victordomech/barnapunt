
window.onload = function() {
    viewHistorial();
};


function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function validaPrice(num){
    if(num == 0 || num.includes(",")==true){
        return false;
    }
    else{
        return true;
    }

}


function viewHistorial(){
    var rows;
    $.ajax({
            type: "POST",
            async: false,
            url: "clases/controller.php", 
            data: "action=7&index="+getParameterByName('id'),
            datatype: "json",

            success: function (dataSuccess) {
                rows=JSON.parse(dataSuccess);
            },
            error: function (error){

            }
    });
    $("#showHistorial div").remove();
    for(i=0;i<rows.length;i++){
        var div= "<div class='list-group'><p class='list-group-item active'>"+(rows[i].servicio)+"<i class='fa fa-trash deleteHist' aria-hidden='true' ></i><span class='right'>"+(rows[i].abonado)+" €</span></p><div class='list-group-item'>"+rows[i].dia+"</div>";
        div+="<div class='list-group-item'><p class='list-group-item-text'>"+rows[i].descripcion+"</p>";
        div+="</div><a class='list-group-item active' href='"+rows[i].factura+"' target='_blank'>Si quieres descargar la factura pincha aqui <span><i class='fa fa-file-pdf-o derecha' aria-hidden='true'></i></span></a></div>";
        $("#showHistorial").append(div);
    };
}