
$("#politicaButton").click(function(){
    $("#politica").show(1500);
    $("#cita").hide(1500);
});
$(".exitPolitica").click(function(){
    $("#cita").show(1500);
    $("#politica").hide(1500);
});




/*
* @name: validaName
* @author: Victor Domenech
* @versio: 1.0
* @description: Funcion que valida un texto.
* @date: 2017/03/29
* @params: Pasas el texto que quieres validar
* @return: true si tiene mas de 3 letras y no contiene numeros    ;false: {else}
*
*/
function validaName(nname){

	if( (nname=="")||(nname.length<3)){
		return false;
	}
	for( i=0; i<nname.length;i++){
		if(nname[i]>='0' && nname[i]<='9'){
			return false
		}
	}
	return true;
}

/*
* @name: valida3
* @author: Victor Domenech
* @versio: 1.0
* @description: Funcion que valida un texto que puede contener numeros y otros caracteres.
* @date: 2017/03/29
* @params: Pasas el texto que quieres validar
* @return: true si tiene mas de 3 letras   ;false: {else}
*
*/
function valida3(nname){

	if( (nname=="")||(nname.length<3)){
		return false;
	}
	else{
		return true;
	}
}

/*
* @name: validateDNI
* @author: Victor Domenech
* @versio: 1.0
* @description: Funcion que valida un DNI.
* @date: 2017/03/29
* @params: Pasas el DNI con letra que quieres validar
* @return: 0: si el formato no es valido  ;  1: si la letra no es valida  ;  2: si es correcto
*
*/
function validateDNI(dni) {
    var numero, let, letra;
    var expresion_regular_dni = /^[XYZ]?\d{5,8}[A-Z]$/;

    dni = dni.toUpperCase();

    if(expresion_regular_dni.test(dni) === true){
        numero = dni.substr(0,dni.length-1);
        numero = numero.replace('X', 0);
        numero = numero.replace('Y', 1);
        numero = numero.replace('Z', 2);
        let = dni.substr(dni.length-1, 1);
        numero = numero % 23;
        letra = 'TRWAGMYFPDXBNJZSQVHLCKET';
        letra = letra.substring(numero, numero+1);
        if (letra != let) {
            return 1;
        }else{
            return 2;
        }
    }else{
        return 0;
    }
}

function validaMail(mail){
   var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
    if (regex.test(mail.trim())){
    	return true;
    }
    else{
    	return false;
    }
}
/*
* @name: validaTlf
* @author: Victor Domenech
* @versio: 1.0
* @description: Funcion que valida un telefono
* @date: 2017/03/29
* @params: Pasas el telefono en formato texto que quieres validar
* @return: true si tiene 9 digitos sin letras   ;false: {else}
*
*/
function validaTlf(tlf){
	if( !(/^\d{9}$/.test(tlf)) ) {
  		return false;
  	}
  	else{
  		return true;
  	}
}

/*
* @name: validaSel
* @author: Victor Domenech
* @versio: 1.0
* @description: Funcion que valida un select
* @date: 2017/03/29
* @params: Pasas el value del select que quieres validar
* @return: true si es 0   ;false: {else}
*
*/
function validaCheck(box){
	alert(box.attr('checked'));
	return box.attr("checked");
}


function validaSel(sel){
	if(sel.val()=="seleccionar"){
		return false;
	}
	else{
		return true;
	}
}

function validaCP(cp){
    if(cp.lenght!=5) {
        return false;
    }
    for( i=0; i<cp.length;i++){
        if(cp[i]>='0' && cp[i]<='9'){
        }
        else{
            return false;
        }
    }
    return true;
}

//************************* JQUERY *************************\\


function validaForm(){
	var flag= true;
	var inputs= $("#citar input").each(function(){
    	if($(this).attr("name")=="name" || $(this).attr("name")=="surname"){
    		if(validaName($(this).val().trim())==false){
    			flag=false;
    			$(this).removeClass("noerror");
    			$(this).addClass("error");
    		}
    		else{
    			$(this).removeClass("error");
    			$(this).addClass("noerror");
    		}
    	}
    	else if($(this).attr("name")=="telephone"){
    		if(validaTlf($(this).val().trim())==false){
    			flag=false;
    			$(this).removeClass("noerror");
    			$(this).addClass("error");
    		}
    		else{
    			$(this).removeClass("error");
    			$(this).addClass("noerror");
    		}
    	}
    	else if($(this).attr("name")=="email"){
    		if(validaMail($(this).val().trim())==false){
				flag=false;
				$(this).removeClass("noerror");
				$(this).addClass("error");
    		}
    		else{
    			$(this).removeClass("error");
    			$(this).addClass("noerror");
    		}
    	}
    	else if($(this).attr("name")=="conditions"){
    		if( $("#conditions").is(":checked")==false){
				flag=false;
				$("this").removeClass("noerror");
				$("this").addClass("error");
			}
			else{
				$("this").removeClass("error");
    			$("this").addClass("noerror");
    		}
  
    	}
    });
    var select= $("#citar select").each(function(){
        if($(this).attr("name")=="services"){
            if(validaSel($(this))==false){
                flag=false;
                $(this).removeClass("noerror");
                $(this).addClass("error");
            }
            else{
                $(this).removeClass("error");
                $(this).addClass("noerror");
            }
        }
    });
    if(flag==false){
        return false;
    }
    else
    {
        return true;    
    }
}

function validaContact(){
    var flag= true;
    var inputs= $("#contact input").each(function(){
        if($(this).attr("name")=="name2" || $(this).attr("name")=="surname2"){
            if(validaName($(this).val().trim())==false){
                flag=false;
                $(this).removeClass("noerror");
                $(this).addClass("error");
            }
            else{
                $(this).removeClass("error");
                $(this).addClass("noerror");
            }
        }
        else if($(this).attr("name")=="telephone2"){
            if(validaTlf($(this).val().trim())==false){
                flag=false;
                $(this).removeClass("noerror");
                $(this).addClass("error");
            }
            else{
                $(this).removeClass("error");
                $(this).addClass("noerror");
            }
        }
        else if($(this).attr("name")=="email2"){
            if(validaMail($(this).val().trim())==false){
                flag=false;
                $(this).removeClass("noerror");
                $(this).addClass("error");
            }
            else{
                $(this).removeClass("error");
                $(this).addClass("noerror");
            }
        }
        else if($(this).attr("name")=="conditions2"){
            if( $("#conditions2").is(":checked")==false){
                flag=false;
                $(this).removeClass("noerror");
                $(this).addClass("error");
            }
            else{
                $(this).removeClass("error");
                $(this).addClass("noerror");
            }
  
        }
    });
    if(flag==false){
        return false;
    }
    else{
        return true;    
    }
}

function login(){
    var flag= true;
    var data=[];
    var inputs= $("#login input").each(function(){
        if($(this).attr("name")=="userlogin" || $(this).attr("name")=="passwordlogin"){
            data.push($(this).val());
            if(valida3($(this).val().trim())==false){
                flag=false;
                $(this).removeClass("noerror");
                $(this).addClass("error");
            }
            else{
                $(this).removeClass("error");
                $(this).addClass("noerror");
            }
        }
      
    });
   if(flag==true){
        var outPutData;
        var jsonData = JSON.stringify(data);

        $.ajax({
            type: "POST",
            async: false,
            url: "clases/controller.php", 
            data: "action=8&data="+jsonData,
            datatype: "json",
            success: function (dataSuccess) {
                outPutData=dataSuccess;
            },
            error: function (error){
                $("#mensj p").remove();
                $("#mensj").append("<p class='errorText'> ERROR: intentelo de nuevo o mas tarde</p>");

            }
        });
        if(outPutData!=false){
            window.location="clases/login.php";
        }
        else{
            $("#mensj p").remove();
            $("#mensj").append("<p class='errorText'> Usuario o contraseña incorrrectos</p>");
        }
    }

}

function changePasss(){
    var flag= true;
    var data=[];
    var inputs= $("#changePass input").each(function(){
        if($(this).attr("name")=="actualPass" || $(this).attr("name")=="newPass" || $(this).attr("name")=="newPassRepeat"){
            data.push($(this).val());
            if(valida3($(this).val().trim())==false){
                flag=false;
                $(this).removeClass("noerror");
                $(this).addClass("error");
            }
            else{
                $(this).removeClass("error");
                $(this).addClass("noerror");
            }
        }
      
    });
    if(data[1]!=data[2]){
        flag=false;
        $("#newPassRepeat").removeClass("noerror");
        $("#newPassRepeat").addClass("error");
        $("#mensj p").remove();
        $("#mensj").append("<p class='errorText'> Las contraseñas no coinciden</p>");
    }

   if(flag==true){

        $.ajax({
            type: "POST",
            async: false,
            url: "clases/controller.php", 
            data: "action=13&data=" + data[1] + "&psw=" + data[0],
            datatype: "json",
            success: function (dataSuccess) {
                outPutData=dataSuccess;
            },
            error: function (error){

            }
        });
        if(outPutData==false){
            $("#actualPass").removeClass("noerror");
            $("#actualPass").addClass("error");
            $("#mensj p").remove();
            $("#mensj").append("<p class='errorText'> Las contraseñas actual no es correcta</p>");

        }
        else{
            $("#mensj p").remove();
            $("#mensj").append("<p class='noErrorText'> La contraseña se ha modificado correctamente</p>");
        }
      
    }
}

function registerNews(){
    var flag= true;
    var data=[];
        data.push($('#inputEmail').val().trim());
        if(validaMail($('#inputEmail').val().trim())==false){
            flag=false;
            $('#inputEmail').removeClass("noerror");
            $('#inputEmail').addClass("error");
            $('#botEmail').removeClass("noerror");
            $('#botEmail').addClass("error");

        }
        else{
            $('#inputEmail').removeClass("error");
            $('#inputEmail').addClass("noerror");
            $('#botEmail').removeClass("error");
            $('#botEmail').addClass("noerror");
        }
    if(flag==true){
        var rows;
        var jsonData = JSON.stringify(data);
        $.ajax({
            type: "POST",
            async: false,
            url: "clases/controller.php", 
            data: "action=17&data="+jsonData,
            datatype: "json",
            success: function (dataSuccess) {
                rows=(dataSuccess);
            },
            error: function (error){

            }
        });
        if(rows==false){
            alert('Este correo ya esta suscrito.');
        }
        else{
             alert('Suscrito satisfactoriamente');
        }
    }
}




