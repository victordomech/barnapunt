window.onload = function() {
    viewUsers();
};

$("#userVis").click(function(){
    $("#showUsers").show();
    $("#showAddUsers").hide();
    $("#showCars").hide();
});

$("#userAdd").click(function(){
    $("#showAddUsers").show();
    $("#showUsers").hide();
    $("#showCars").hide();
});


$("#carVis").click(function(){
    $("#showCars").show();
    $("#showUsers").hide();
    $("#showAddUsers").hide();
    viewVehicles();
});

/*
* @name: validaName
* @author: Victor Domenech
* @versio: 1.0
* @description: Funcion que valida un texto.
* @date: 2017/03/29
* @params: Pasas el texto que quieres validar
* @return: true si tiene mas de 3 letras y no contiene numeros    ;false: {else}
*
*/
function validaName(nname){

	if( (nname=="")||(nname.length<3)){
		return false;
	}
	for( i=0; i<nname.length;i++){
		if(nname[i]>='0' && nname[i]<='9'){
			return false
		}
	}
	return true;
}

/*
* @name: valida3
* @author: Victor Domenech
* @versio: 1.0
* @description: Funcion que valida un texto que puede contener numeros y otros caracteres.
* @date: 2017/03/29
* @params: Pasas el texto que quieres validar
* @return: true si tiene mas de 3 letras   ;false: {else}
*
*/
function valida3(nname){

	if( (nname=="")||(nname.length<3)){
		return false;
	}
	else{
		return true;
	}
}

/*
* @name: validaMail
* @author: Victor Domenech
* @versio: 1.0
* @description: Funcion que valida un mail.
* @date: 2017/06/08
* @params: Pasas el texto que quieres validar
* @return: true si es un mail.
*
*/
function validaMail(mail){
   var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
    if (regex.test(mail.trim())){
    	return true;
    }
    else{
    	return false;
    }
}

/*
* @name: validaTlf
* @author: Victor Domenech
* @versio: 1.0
* @description: Funcion que valida un telefono
* @date: 2017/03/29
* @params: Pasas el telefono en formato texto que quieres validar
* @return: true si tiene 9 digitos sin letras   ;false: {else}
*
*/
function validaTlf(tlf){
    if( !(/^\d{9}$/.test(tlf)) ) {
        return false;
    }
    else{
        return true;
    }
}

/*
* @name: validaCP
* @author: Victor Domenech
* @versio: 1.0
* @description: Funcion que valida un codigo postal.
* @date: 2017/06/08
* @params: Pasas el texto que quieres validar
* @return: true si es un cp español.
*
*/
function validaCP(cp){
	if (cp >= 1 && cp <= 52999 && cp.length == 5){ 
		return true; 
	} 
	else { 
		return false; 
	}  
}


function register(){
	var flag= true;
	var data=[];
    var inputs= $("#addUser input").each(function(){
    	data.push($(this).val());
        if($(this).attr("name")=="name3"){
            if(validaName($(this).val().trim())==false){
                flag=false;
                $(this).removeClass("noerror");
                $(this).addClass("error");
            }
            else{
                $(this).removeClass("error");
                $(this).addClass("noerror");
            }
        }
        else if($(this).attr("name")=="surname3"){
            $(this).removeClass("noerror");
            $(this).removeClass("error");
            if($(this).val()!=""){
                if(validaName($(this).val().trim())==false){
                    flag=false;
                    $(this).removeClass("noerror");
                    $(this).addClass("error");
                }
                else{
                    $(this).removeClass("error");
                    $(this).addClass("noerror");
                }
            }
        }
        else if($(this).attr("name")=="password3"){
            if(valida3($(this).val().trim())==false){
                flag=false;
                $(this).removeClass("noerror");
                $(this).addClass("error");
            }
            else{
                $(this).removeClass("error");
                $(this).addClass("noerror");
            }
        }
        else if($(this).attr("name")=="email3"){
            if(validaMail($(this).val().trim())==false){
                flag=false;
                $(this).removeClass("noerror");
                $(this).addClass("error");
            }
            else{
                $(this).removeClass("error");
                $(this).addClass("noerror");
            }
        }
        else if($(this).attr("name")=="telephone3"){
            $(this).removeClass("noerror");
            $(this).removeClass("error");
            if($(this).val()!=""){
                if(validaTlf($(this).val().trim())==false){
                    flag=false;
                    $(this).removeClass("noerror");
                    $(this).addClass("error");
                }
                else{
                    $(this).removeClass("error");
                    $(this).addClass("noerror");
                }
            }
        }
        else if($(this).attr("name")=="postal_c3"){
            if(validaCP($(this).val().trim())==false){
                flag=false;
                $(this).removeClass("noerror");
                $(this).addClass("error");
            }
            else{
                $(this).removeClass("error");
                $(this).addClass("noerror");
            }
        }
    });
    if(flag==true){
        var rows;
        var jsonData = JSON.stringify(data);
        $.ajax({
            type: "POST",
            async: false,
            url: "clases/controller.php", 
            data: "action=1&data="+jsonData,
            datatype: "json",
            success: function (dataSuccess) {
                rows=JSON.parse(dataSuccess);
            },
            error: function (error){

            }
        });
		if(rows==false){
            $("#mens p").remove();
			$("#mens").append("<p class='errorText'>ESTE CORREO YA EXISTE </p>");
		}
        else{
            selectUser(rows.id_user,rows.nombre,rows.apellidos,rows.mail,rows.telefono,rows.codigo_p);
        }
	}
}

function viewUsers(){
    var rows;
    $.ajax({
            type: "POST",
            async: false,
            url: "clases/controller.php", 
            data: "action=2",
            datatype: "json",

            success: function (dataSuccess) {
                rows=JSON.parse(dataSuccess);
            },
            error: function (error){

            }
    });
    var tr;
    $("#tableBody tr").remove();
    for(i=0;i<rows.length;i++){
        if(rows[i].telefono==0){
            rows[i].telefono="";
        }
        if(i%2==0){
            tr="<tr class='warning' >";
        }
        else{
              tr="<tr class='active'>";
        }
        $("#tableBody").append(tr+"<td onclick='selectUser("+rows[i].user_id+",\""+rows[i].nombre+
            "\",\""+rows[i].apellidos+"\",\""+rows[i].mail+"\",\""+rows[i].telefono+"\",\""+rows[i].codigo_p+"\" )'>"+rows[i].nombre+"</td><td onclick='selectUser("+rows[i].user_id+",\""+rows[i].nombre+
            "\",\""+rows[i].apellidos+"\",\""+rows[i].mail+"\",\""+rows[i].telefono+"\",\""+rows[i].codigo_p+"\" )'>"
        +rows[i].apellidos+"</td><td onclick='selectUser("+rows[i].user_id+",\""+rows[i].nombre+
            "\",\""+rows[i].apellidos+"\",\""+rows[i].mail+"\",\""+rows[i].telefono+"\",\""+rows[i].codigo_p+"\" )'>"+rows[i].mail+"</td><td onclick='selectUser("+rows[i].user_id+",\""+rows[i].nombre+
            "\",\""+rows[i].apellidos+"\",\""+rows[i].mail+"\",\""+rows[i].telefono+"\",\""+rows[i].codigo_p+"\" )'>"
        +rows[i].telefono+"</td><td onclick='selectUser("+rows[i].user_id+",\""+rows[i].nombre+
            "\",\""+rows[i].apellidos+"\",\""+rows[i].mail+"\",\""+rows[i].telefono+"\",\""+rows[i].codigo_p+"\" )'>"+rows[i].codigo_p+"</td><td class='text-center'><i class='fa fa-trash ' onclick=\"deleteUser('"+rows[i].user_id+"')\" aria-hidden='true'></i></td></tr>");
    };
}

function selectUser(id,nombre,apellidos,mail,telefono,codigo){
    window.location="userAdmin.php?id="+id+"&nombre="+nombre+"&apellidos="+apellidos+"&mail="+mail+"&telefono="+telefono+"&codigo="+codigo;
}

function selectVehicle(id){
    window.location="historialAdmin.php?id="+id;
}


function searchUser(){
    var rowss;
    var data=[];
    var inputs= $("#searchUsers input").each(function(){
        data.push($(this).val());
    });
    var jsonData = JSON.stringify(data);
    $.ajax({
            type: "POST",
            async: false,
            url: "clases/controller.php", 
            data: "action=3&data="+jsonData,
            datatype: "json",

            success: function (dataSuccess) {
                rowss=JSON.parse(dataSuccess);

            },
            error: function (error){
            }
    });
     var tr;
    $("#tableBody tr").remove();
    for(i=0;i<rowss.length;i++){
        if(rowss[i].telefono==0){
            rowss[i].telefono="";
        }
          if(i%2==0){
            tr="<tr class='warning' onclick='selectUser("+rowss[i].user_id+",\""+rowss[i].nombre+
            "\",\""+rowss[i].apellidos+"\",\""+rowss[i].mail+"\",\""+rowss[i].telefono+"\",\""+rowss[i].codigo_p+"\" )'>";
        }
        else{
              tr="<tr class='active' onclick='selectUser("+rowss[i].user_id+",\""+rowss[i].nombre+
              "\",\""+rowss[i].apellidos+"\",\""+rowss[i].mail+"\",\""+rowss[i].telefono+"\",\""+rowss[i].codigo_p+"\" )'>";
        }
        $("#tableBody").append(tr+"<td>"+rowss[i].nombre+"</td><td>"
        +rowss[i].apellidos+"</td><td>"+rowss[i].mail+"</td><td>"
        +rowss[i].telefono+"</td><td>"+rowss[i].codigo_p+"</td><td class='text-center'><i class='fa fa-trash ' onclick=\"deleteUser('"+rowss[i].user_id+"')\" aria-hidden='true'></i></td></tr>");
    };
}

function viewVehicles(){
    var rows;
    $.ajax({
            type: "POST",
            async: false,
            url: "clases/controller.php", 
            data: "action=10",
            datatype: "json",

            success: function (dataSuccess) {
                rows=JSON.parse(dataSuccess);
            },
            error: function (error){

            }
    });
    var tr;
    $("#tableBody2 tr").remove();
    for(i=0;i<rows.length;i++){
        if(i%2==0){
            tr="<tr class='warning' onclick='selectVehicle("+"\""+rows[i].matricula_id+"\""+")'>";
         }
        else{
            tr="<tr class='active' onclick='selectVehicle("+"\""+rows[i].matricula_id+"\""+")'>";
        }
        $("#tableBody2").append(tr+"<td>"+rows[i].matricula_id+"</td><td>"
        +rows[i].marca+"</td><td>"+rows[i].type+"</td><td>"
        +rows[i].modelo+"</td><td>"+rows[i].year+"</td></tr>");
    };
}

function searchVehicles(){
    var rows;
    var data=[];
    var inputs= $("#searchVehicles input").each(function(){
        data.push($(this).val());
    });
    var jsonData = JSON.stringify(data);
    $.ajax({
            type: "POST",
            async: false,
            url: "clases/controller.php", 
            data: "action=11&data="+jsonData,
            datatype: "json",

            success: function (dataSuccess) {
                rows=JSON.parse(dataSuccess);

            },
            error: function (error){
            }
    });
    var tr;
    $("#tableBody2 tr").remove();
    for(i=0;i<rows.length;i++){
        if(i%2==0){
            tr="<tr class='warning' onclick='selectVehicle("+"\""+rows[i].matricula_id+"\""+")'>";
         }
        else{
            tr="<tr class='active' onclick='selectVehicle("+"\""+rows[i].matricula_id+"\""+")'>";
        }
        $("#tableBody2").append(tr+"<td>"+rows[i].matricula_id+"</td><td>"
        +rows[i].marca+"</td><td>"+rows[i].type+"</td><td>"
        +rows[i].modelo+"</td><td>"+rows[i].year+"</td></tr>");
    };
}

function deleteUser(id){
    var r = confirm("Estas seguro que deseas eliminar este Usuario? (Se eliminaran los vehiculos y el historial completo de estos)");
    if (r == true) {
        $.ajax({
                type: "POST",
                async: false,
                url: "clases/controller.php", 
                data: "action=16&id="+id,
                datatype: "json",

                success: function (dataSuccess) {
                    rows=JSON.parse(dataSuccess);

                },
                error: function (error){
                }
        });
        if(rows==true){
            location.reload(true);
        }
    }
}

