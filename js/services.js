(function(){
var header = document.querySelector('.header');
    
    header.onclick = function() {
        header.classList.toggle('menu-opened');
    }
}());



$(window).bind('scroll', function () {
    if ($(window).width()> 992 && $(window).scrollTop() > 491) {
        $('.nav_fix').addClass('fixed');
        $('#fixmenu').addClass('displayyes');
    }
    else if ($(window).width()<= 992 && $(window).width()> 768 && $(window).scrollTop() > 116) {
        $('.nav_fix').addClass('fixed');
        $('#fixmenu').addClass('displayyes');
    } 

    else if ($(window).width()<= 768 && $(window).scrollTop() > 114) {
        $('.nav_fix').addClass('fixed');
        $('#topbar').addClass('displayyes');
    } 
    else {
        $('.nav_fix').removeClass('fixed');
        $('.nav_fix').removeClass('displayyes');
        $('#topbar').removeClass('displayyes');
    }
});
