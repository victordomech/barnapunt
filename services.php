<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="description" content="Centro de mantenimiento de coches y motos multimarca, venta de recambios,neumáticos y accesorios de coches y motos con la mejor relación calidad-precio, barnapuncar" >
  <meta name="keywords" content="turismo, coche, vehiculo, neumáticos, servicios, mecánica,recambios,compra/venta,Barnapuntcar,BarnaPunt,barnapunt">
  <meta name="application-name" content="Barnapuntcar">

    <title>BarnaPunt</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.min.css" rel="stylesheet">

    <script src="https://use.fontawesome.com/a81c118adb.js"></script>
  </head>
  <body>
    <div class="container-fluid">
     <?php
        session_start();
        require_once 'code/header.php';
     ?>
 <main>
    <div Id="clearBoth"></div>
    <div class="row" id="servicios">
    <div class="container1250">
      <div class="col-md-6">
         <div class="row row_padding3" >
            <div class="col-md-6">
              <span class="entero">
              <a href="#mantenimiento" ><h3 class="titulos_servicios iz">MANTENIMIENTO</h3></a>
              </span>
              <span class="entero">
              <a href="#mecanica" ><h3 class="titulos_servicios iz">MECÁNICA</h3></a>
              </span>
              <span class="entero">
              <a href="#electricidad" > <h3 class="titulos_servicios iz">ELECTRICIDAD</h3></a>
              </span>
              <span class="entero">
              <a href="#diagnosis" ><h3 class="titulos_servicios iz">DIAGNOSIS</h3></a>
              </span>
           </div>
           <div class="col-md-6">
              <span class="entero">
              <a href="#climatizacion" ><h3 class="titulos_servicios iz">CLIMATIZACIÓN</h3></a>
              </span>
              <span class="entero">
              <a href="#frenos" ><h3 class="titulos_servicios iz">FRENOS</h3></a>
              </span>
              <span class="entero">
              <a href="#neumaticos" ><h3 class="titulos_servicios iz">NAUMÁTICOS</h3></a>
              </span>
              <span class="entero">
              <a href="#multimedia" ><h3 class="titulos_servicios iz">MONTAJE MULTIMEDIA</h3></a>
              </span>
           </div>
         </div>
       </div>
       <div class="col-md-6">
            <div class="der"id="carRep">
            <img src="img/repairing-car.svg" id="img_coche" >
        </div>
    </div> 
  </div>
  </div>
  <div Id="clearBoth"></div>
  <div class="row nav_fix" id="fixmenu">
      <div class="col-sm-6 ">
          <ul class="nav navbar-nav" id="nav_ser">
            <a href="#mantenimiento"><li class="ser"> <img src="img/mantenimiento.svg"  ></li></a>
            <a href="#mecanica"><li class="ser"> <img src="img/Mecanica.svg"  ></li></a>
            <a href="#electricidad"><li class="ser"> <img src="img/electricidad.svg"  ></li></a>
            <a href="#diagnosis"><li class="ser"> <img src="img/diagnosis.svg"  ></li></a>
          </ul>
      </div>
      <div class="col-sm-6 ">
          <ul class="nav navbar-nav" id="nav_ser">
            <a href="#climatizacion"><li class="ser"> <img src="img/climatizacion.svg" ></li></a>
            <a href="#frenos"><li class="ser"> <img src="img/frenos.svg"  ></li></a>
            <a href="#neumaticos"><li class="ser"> <img src="img/Neumatico.svg"  ></li></a>
            <a href="#multimedia"><li class="ser"> <img src="img/Multimedia.svg"  ></li></a>
          </ul>
        </div>
  </div>
  
<div class="window" id="topbar">
  <div class="header" onclick="header()">
    <div class="burger-container">
      <div id="burger">
        <div class="bar topBar"></div>
        <div class="bar btmBar"></div>
      </div>
    </div>
    <ul class="menu">
      <li class="menu-item"><a href="#mantenimiento"></span>MANTENIMIENTO</a></li>
      <li class="menu-item"><a href="#mecanica">MECÁNICA</a></li>
      <li class="menu-item"><a href="#electricidad">ELECTRICIDAD</a></li>
      <li class="menu-item"><a href="#diagnosis">DIAGNOSIS</a></li>
      <li class="menu-item"><a href="#climatizacion">CLIMATIZACIÓN</a></li>
      <li class="menu-item"><a href="#frenos">FRENOS</a></li>
      <li class="menu-item"><a href="#neumaticos">NAUMÁTICOS</a></li>
      <li class="menu-item"><a href="#multimedia">MONTAJE MULTIMEDIA</a></li>
    </ul>
  </div>
  </div>



  
  <article class="grisClarito">
    <div class=" row container1250 row_padding" id="mantenimiento">
      <h3 class="text-center titulos colorfont_gris" >MANTENIMIENTO</h3>
      <div class="col-md-6 row_padding">
        <h3>GARANTÍA OFICIAL GARANTIZADA</h3>
        <p>
         Te ofrecemos un servicio de mantenimiento <b>multimarca conservando la garantía del fabricante*</b> del turismo a un precio mucho más competitivo. Además, revisamos 30 puntos vitales de seguridad y rendimiento de tu vehículo a un precio cerrado, sin sorpresas:
        <br>
         *La extensión de la garantía deberá validarse con el contrato del fabricante.
        </p>
        

      </div>
      <div class="col-md-6 row_padding">
        <h3>SERVICIOS DE MANTENIMIENTO DEL AUTOMÓVIL</h3>
         <ul>
            <li><p>Sustitución de aceite y filtros</p></li>
            <li><p>Revisión oficial del coche según parámetros del fabricante</p></li>
            <li><p>Reset testigo inspección técnica </p></li>
            <li><p>Regulación y ajuste del alumbrado del automóvil</p></li>
            <li><p>Sustitución de baterías, bujías y calentadores del automóvil</p></li>
            <li><p>Sustitución de la correa de distribución del automóvil</p></li>
            <li><p>Amortiguadores y suspensión del automóvil</p></li>
            <li><p>Sistemas anticontaminación: FAP, DPF</p></li>
            <li><p>Correa de accesorios o servicio</p></li>
            <li><p>Revisión del estado de las escobillas limpiaparabrisas y del nivel de líquido </p></li>
          </ul>
      </div>
    </div>
  </article>

  <article class="gris">
    <div class=" row container1250 row_padding" id="mecanica">
      <h3 class="text-center titulos colorfont_amarillo" >MECÁNICA</h3>
      <div class="col-md-6 row_padding">
        <h3 class="colorfont_amarillo">TODO Y MÁS</h3>
        <p class="colorfont_grisclarito">
         Te ofrecemos un <b>servicio integral de reparación del automóvil</b> siguiendo un estricto programa de formación que nos permite estar al día en las nuevas motorizaciones y tecnologías más avanzadas.
        </p>
        

      </div>
      <div class="col-md-6 row_padding">
        <h3 class="colorfont_amarillo">SERVICIOS DE MECÁNICA DEL AUTOMÓVIL</h3>
         <ul class="colorfont_grisclarito">
            <li><p>Sistemas de distribución</p></li>
            <li><p>Culatas</p></li>
            <li><p>Embragues</p></li>
            <li><p>Cambios de aceite, neumáticos, etc </p></li>
            <li><p>Motor</p></li>
            <li><p>Sistema de inyección</p></li>
            <li><p>Transmisiones y diferencial</p></li>
            <li><p>Escapes, catalizadores y sistemas anticontaminación</p></li>
            <li><p>Sistemas de sobrealimentación: turbocompresores</p></li>
          </ul>
      </div>
    </div>
  </article>
    
  <article class="grisClarito">
    <div class=" row container1250 row_padding" id="electricidad">
      <h3 class="text-center titulos colorfont_gris">ELECTRICIDAD</h3>
      <div class="col-md-6 row_padding">
        <h3>EL FUTURO YA ESTÁ AQUÍ</h3>
        <p>
         La parte electrónica es y será la que evolucionará más en los próximos modelos de vehículos en un futuro cercano. Por ello, en BarnaPuntCar estamos siempre en constante formación.
         </p>
        

      </div>
      <div class="col-md-6 row_padding">
        <h3>SERVICIOS ELECTRICIDAD DEL AUTOMÓVIL</h3>
         <ul>
            <li><p>Sistema de arranque y carga</p></li>
            <li><p>Electricidad telemática del automóvil</p></li>
            <li><p>Electricidad multimedia del automóvil</p></li>
            <li><p>Revisión y reparación de los sistemas de confort</p></li>
            <li><p>Sustitución de baterías, bujías y calentadores del automóvil</p></li>
            <li><p>Sustitución de la correa de distribución del automóvil</p></li>
            <li><p>Revisión y reparación de los sistemas motorizados (ventanas, techos, puertas y asientos)</p></li>
          </ul>
      </div>
    </div>
  </article>

  <article class="gris">
    <div class=" row container1250 row_padding" id="diagnosis">
      <h3 class="text-center titulos colorfont_amarillo">DIAGNOSIS</h3>
      <div class="col-md-6 row_padding">
        <h3 class="colorfont_amarillo">PREVENIR ES EVITAR</h3>
        <p class="colorfont_grisclarito">
         Para un diagnóstico previo de coche y evitar reparaciones de su vehículo, disponemos de equipos de diagnosis multimarca y contamos con el software específico en información técnica con datos de referencia para una verificación más detallada y eficaz.
        </p>
        

      </div>
      <div class="col-md-6 row_padding">
        <h3 class="colorfont_amarillo">SERVICIOS DE DIAGNOSIS DEL AUTOMÓVIL</h3>
         <ul class="colorfont_grisclarito">
            <li><p>Gestión del motor</p></li>
            <li><p>Transmisiones automáticas</p></li>
            <li><p>Sistemas de tracción integral</p></li>
            <li><p>Sistemas de seguridad activa y pasiva
</p></li>
            <li><p>Sistemas de confort
</p></li>
            <li><p>Sistema de inyección</p></li>
            <li><p>Sistemas anticontaminación</p></li>
          </ul>
      </div>
    </div>
  </article>

  <article class="grisClarito">
    <div class=" row container1250 row_padding" id="climatizacion">
      <h3 class="text-center titulos colorfont_gris">CLIMATIZACIÓN</h3>
      <div class="col-md-6 row_padding">
        <h3>FRÍO Y CALOR</h3>
        <p>
         Efectuamos cargas del sistema de climatización mediante tecnología de última generación y realizamos un diagnóstico exhaustivo de tu vehículo para verificar la correcta eficacia del sistema de climatización.
         </p>
        

      </div>
      <div class="col-md-6 row_padding">
        <h3>SERVICIOS DE CLIMATIZACIÓN DEL AUTOMÓVIL</h3>
         <ul>
            <li><p>Carga de aire acondicionado R134</p></li>
            <li><p>Reparación de compresores</p></li>
            <li><p>Reparación de radiadores</p></li>
            <li><p>Reparación de evaporadores</p></li>
            <li><p>Válvulas de expansión</p></li>
            <li><p>Presostatos</p></li>
            <li><p>Condensadores</p></li>
            <li><p>Filtros secadores/deshidratadores</p></li>
          </ul>
      </div>
    </div>
  </article>

  <article class="gris">
    <div class=" row container1250 row_padding" id="frenos">
      <h3 class="text-center titulos colorfont_amarillo">FRENOS</h3>
      <div class="col-md-6 row_padding">
        <h3 class="colorfont_amarillo">STOP & GO</h3>
        <p class="colorfont_grisclarito">
        El sistema de frenado de tu vehículo debe estar siempre en <b>perfecto estado</b> para garantizarte una conducción segura. Por ello, realizamos una revisión integral de todo el sistema de frenado. <b>La seguridad es lo primero.</b>
        </p>
        

      </div>
      <div class="col-md-6 row_padding">
        <h3 class="colorfont_amarillo">SERVICIOS FRENO DEL AUTOMÓVIL</h3>
         <ul class="colorfont_grisclarito">
            <li><p>Sustitución de discos y pastillas de freno</p></li>
            <li><p>Diagnosis de sistemas ABS</p></li>
          </ul>
      </div>
    </div>
  </article>

  <article class="grisClarito">
    <div class=" row container1250 row_padding" id="neumaticos">
      <h3 class="text-center titulos colorfont_gris" >NEUMÁTICOS</h3>
      <div class="col-md-6 row_padding">
        <h3>SOMOS TU ESPECIALISTA</h3>
        <p>
        El <b>buen estado de los neumáticos </b>es vital para la seguridad. En RODI MOTOR SERVICES somos especialistas en el control y la selección de los neumáticos más adecuados a tus necesidades. Disponemos de las mejores marcas de neumáticos y contamos con el asesoramiento técnico de nuestros profesionales.
         </p>
        

      </div>
      <div class="col-md-6 row_padding" >
        <h3>SERVICIOS NEUMÁTICOS DEL AUTOMÓVIL</h3>
         <ul>
            <li><p>Asesoramiento en neumáticos</p></li>
            <li><p>Reparación de pinchazos de neumáticos </p></li>
            <li><p>Sustituciones de neumáticos </p></li>
            <li><p>Equilibrados de neumáticos 
</p></li>
            <li><p>Alineaciones de neumáticos</p></li>
            <li><p>Verificaciones y control del estado de la presión de neumáticos</p></li>
          </ul>
      </div>
    </div>
  </article>

  <article class="gris">
    <div class=" row container1250 row_padding" id="multimedia">
      <h3 class="text-center titulos colorfont_amarillo">MONTAJE MULTIMEDIA</h3>
      <div class="col-md-6 row_padding">
        <h3 class="colorfont_amarillo">MODERNIZA TU VEHICULO</h3>
        <p class="colorfont_grisclarito">
         ¿Te has comprado o te han regalado una nueva radio o sistema de navegación y no sabes cómo montarlo?
          Si ves que tu coche necesita algún extra tecnológico en la tienda BarnaPuntCar encontras todo tipo de productos como radios, altavoces, sensores, gps... y mucho más si no sabes como montarlo nuestros técnicos en el taller pueden hacerlo.
        </p>
        

      </div>
      <div class="col-md-6 row_padding">
        <h3 class="colorfont_amarillo">SERVICIOS MULTIMEDIA Y ACCESORIOS ELECTRONICOS</h3>
         <ul class="colorfont_grisclarito">
            <li><p>Montaje de radios</p></li>
            <li><p>Montaje de altavoces</p></li>
            <li><p>Sistemas de navegación multimedia ANDROID</p></li>
            <li><p>Montaje de camara trasera</p></li>
            <li><p>Sensor de aparcamientos</p></li>
          </ul>
      </div>
    </div>
  </article>


  </main>
   <div Id="clearBoth"></div>
    <?php
     require_once 'code/footer.php';
     ?>
  </div>

    <script src="js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/services.js"></script>
    <script src="js/validate.min.js"></script>
    <script>
    $(document).ready(function(){
      linkInterno = $('a[href^="#"]');
      linkInterno.on('click',function(e) {
      e.preventDefault();
      var href = $(this).attr('href');
      $('html, body').animate({ scrollTop : $( href ).offset().top+10 }, 1000, 'easeInOutExpo');
      });
    });
  </script>
  <?php include_once("code/analyticstracking.php") ?>
  </body>
</html>