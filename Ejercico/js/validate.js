
/*
* @name: valida3
* @author: Victor Domenech
* @versio: 1.2
* @description: Funcion que valida un texto que puede contener numeros y otros caracteres.
* @date: 2018/03/19
* @params: Pasas el texto que quieres validar
* @return: true si tiene mas de 3 letras y menos de 10   ;false: {else}
*
*/
function valida(field){

	if( (field=="")||(field.length<3)||(field.length>10)){
		return false;
	}

	else{
		return true;
	}
}



//************************* JQUERY *************************\\



function login(){
    var flag= true;
    var data=[];
    var inputs= $("#login input").each(function(){
        if($(this).attr("name")=="userlogin" || $(this).attr("name")=="passwordlogin"){
            data.push($(this).val());
            if(valida($(this).val().trim())==false){
                flag=false;
                $(this).addClass("vdr-error");
                $('#login .vdr-form-error').html( "<p>The fields must have between 3 and 10 characters</p>" ); 
            }
            else{
                $(this).removeClass("vdr-error");
            }
        }
      
    });

   if(flag==true){

        var outPutData;
        var jsonData = JSON.stringify(data);

        $.ajax({
            type: "POST",
            async: false,
            url: "login.php", 
            data: "action=login&data="+jsonData,
            datatype: "json",
            success: function (dataSuccess) {
                outPutData=dataSuccess;
            },
            error: function (error){
            }
        });

        outPutData= JSON.parse(outPutData)

        var msg = outPutData.msg;
        var ajax_status = outPutData.ajax_status;
        var redirect = outPutData.redirect;
        var highlight = outPutData.highlight;

        console.log(outPutData);
        
        if( ajax_status == 200){

            location.href = redirect;
        }

        else if( ajax_status == 403){

            $('#login .vdr-form-error').html( "<p>"+ msg +"</p>" ); 


            if(highlight==true){

                $("#login input").each(function(){
      
                    $(this).addClass("vdr-error");
                });
            }
        }  

        else{
            alert('ERROR');
        }
    }
    else{
        return false;
    }
}

(function($) {

    /*
        ON LOAD
    *******************************************/
    $(function () {

        $( ".vdr-telonPage--right img" ).mouseenter(function() {

          $( 'body' ).addClass( 'vdr_telonOpen' );

        });

    });

})(jQuery);



