<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.5">
    <meta name="description" content="Centro de mantenimiento de coches y motos multimarca, venta de recambios,neumáticos y accesorios de coches y motos con la mejor relación calidad-precio, barnapuncar" >
   <meta name="keywords" content="turismo, coche, vehiculo, neumáticos, servicios, mecánica,recambios,compra/venta,Barnapuntcar,BarnaPunt,barnapunt">
   <meta name="application-name" content="Barnapuntcar">

    <title>BarnaPunt</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/swiper.min.css">
    <link href="css/style.min.css" rel="stylesheet">

    <script src="https://use.fontawesome.com/a81c118adb.js"></script>
  </head>
  	<body>
  	<div class="container-fluid">
  	 <?php
  	    session_start();
  	 	require_once 'code/header.php';
  	 ?>
		<div Id="clearBoth"></div>
	
		<main>
			<div class="row" id="rowBlanca"></div>
			<div Id="clearBoth"></div>
			<div class="row">
				<div class="col-md-12" id="slider">
					<div class="swiper-container" >
			       		<div class="swiper-wrapper">
				            <div class="swiper-slide"><img alt="Barnapuntcar"
						     src="img/gallery/BarnaPunt_fachada3.jpg"></div>
				            <div class="swiper-slide"><img alt="Barnapuntcar"
						     src="img/gallery/BarnaPunt_fachada2.jpg"></div>
				            <div class="swiper-slide"><img alt="Barnapuntcar"
						     src="img/gallery/BarnaPunt_tienda1.jpg"></div>
				            <div class="swiper-slide"><img alt="Barnapuntcar"
						     src="img/gallery/BarnaPunt_tienda2.jpg"></div>
				            <div class="swiper-slide"><img alt="Barnapuntcar"
						     src="img/gallery/BarnaPunt_taller1.jpg"></div>
		        		</div>
	        	<!-- Add Pagination -->
		        		<div class="swiper-pagination"></div>
		        <!-- Add Arrows -->
					    <div class="swiper-button-next"></div>
					    <div class="swiper-button-prev"></div>
	   				</div>
				</div>
			</div>
			<div Id="clearBoth"></div>
			<div class="container1250">
				
				<div class="row" id="row_padding_botones">
					<div id="botones">
						<div class="col-md-6 col-xm-12 col-xs-12 ">
							<button type="button" class="btn btnn btn-block">
								<p class="press">SERVICIOS MULTIMEDIA</p>
							</button>
						</div>
						<div class="col-md-6 col-xm-12 col-xs-12">
							<button type="button" class="btn btnn btn-block">
								<p class="press">TIENDA</p>
							</button>
						</div>
						<div id="clearBoth"></div>
					</div>
				</div>

			</div>
			<div Id="clearBoth"></div>
			
			<div class="row row_padding" id="thumb" >
				<div class="container1250">
					<div class="col-md-12">
						<h3 class="text-center titulos colorfont_amarillo" >EN PROMOCIÓN</h3>
						<div class="row row_margin">
							<div class="col-md-4 thomb_padding">
								<div class="thumbnail">
									<img alt="img cambio aceite" src="img/cambio_aceite.jpg">
									<div class="caption">
										<h3>
											CAMBIO DE ACEITE
										</h3>
										<p>
											Realiza el cambio de aceite de tu vehículo al mejor precio: 10W40 + filtro de aceite por solo 69,90 €. Trabajamos con marcas líderes en lubricantes.
										</p>
										<div class="thumb_text">
											<a class="btn btn-block bot text-center buena_letra" href="promociones.php#aceite">saber más</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 thomb_padding">
								<div class="thumbnail">
									<img alt="img ruedas nuevas" src="img/ruedas_nuevas.jpg">
									<div class="caption">
										<h3>
											SUPER OFERTA DE NEUMATICOS
										</h3>
										<p>
											Aprovecha la mejor oferta que encontraras en neumaticos. Informate sobre nuestra promocion en neumaticos Continental que incluye el montaje y mucho más desde 78'45 € 
										</p>
										<div class="thumb_text">
											<a class="btn btn-block bot text-center buena_letra" href="promociones.php#neumatiocos">saber más</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 thomb_padding">
								<div class="thumbnail ">
									<img alt="img puesta a punto" src="img/camara_trasera.jpg">
									<div class="caption">
										<h3>
											CAMARA TRASERA
										</h3>
										<p>
											¿Estas pensando en ponerte una pantalla multimedia? No te pierdas nuestra mejor oferta que incluye de regalo una camara trasera y el montaje completo, valorado en 200€. 
										</p>
										<div class="thumb_text">
											<a class="btn btn-block bot text-center buena_letra" href="promociones.php#camara">saber más</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 thomb_padding">
								<div class="thumbnail">
									<img alt="img cambio aceite" src="img/amortiguadores.png">
									<div class="caption">
										<h3>
											4 X 3 AMORTIGUADORES
										</h3>
										<p>
											La función de los amorguadores es mantener la adhesión del coche a la calzada, reduciendo las oscilaciones del vehículo.<br>
		                    				Forman parte del triángulo de seguridad del vehículo : frenado, neumáticos, amortiguadores. Con nuestra oferta llevate 4 por el precio de 3.
										</p>
										<div class="thumb_text">
											<a class="btn btn-block bot text-center buena_letra" href="promociones.php#aceite">saber más</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 thomb_padding">
								<div class="thumbnail ">
									<img alt="img puesta a punto" src="img/alfombrillas.jpg">
									<div class="caption">
										<h3>
											ALFOMBRILLAS A MEDIDA 
										</h3>
										<p>
											Ven y llevate el pack de alfombrillas a medida para tu vehiculo desde 19'95€. 
										</p>
										<div class="thumb_text">
											<a class="btn btn-block bot text-center buena_letra" href="promociones.php#alfombrillas">saber más</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-4 thomb_padding">
								<div class="thumbnail ">
									<img alt="img puesta a punto" src="img/cascos.png">
									<div class="caption">
										<h3>
											CASCOS DE MOTO
										</h3>
										<p>
											Disfruta de nuestra seccion de cascos de moto en nuestra tienda, tenemos de todos los tipos, colores y medidas. 
										</p>
										<div class="thumb_text">
											<a class="btn btn-block bot text-center buena_letra" href="promociones.php#cascos">saber más</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div Id="clearBoth"></div>

			<div class="row row_padding grisClarito" id="tienda">
				<div class="container1250">
					<h3 class="text-center titulos colorfont_gris">DESCUBRE NUESTRA TIENDA</h3>
					<div class="row thomb_padding tiendaRow">
						<div class="col-md-6 col-md-push-6 ">
							<div class="img_Container">
								<img src="img/gallery/BarnaPunt_tienda1.jpg">
							</div>
						</div>
						<div class="col-md-6 col-md-pull-6 tiendaSection1">
							<div class="text-center tiendaSectionText">
								<br><p>
								En la tienda encontrarás todo lo que buscas para tu vehículo. Trabajamos con las marcas más competitivas del mercado para ofrecerte los productos más adecuados a tus necesidades. Con una superficie de 100 m2. 
								</p>
								<p><br>
								Cuenta también con sala de espera, donde podrás estar cómodo y entretenido tomando un café mientras trabajamos en el vehículo.
								</p>
							</div>
						</div>
					</div>
					<div class="row thomb_padding tiendaRow">
						<div class="col-md-6 ">
							<div class="img_Container">
								<img src="img/gallery/BarnaPunt_tienda2.jpg">
							</div>
						</div>
						<div class="col-md-6 tiendaSection2">
							<div class="text-center tiendaSectionText">
								<div class="border_tienda">
									<p class="visita_tienda">
									¡VISITA NUESTRA TIENDA PARA ENCONTRAR LOS MEJORES ARTÍCULOS PARA TU VEHICULO!
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div Id="clearBoth"></div>
		</main>
		<div Id="clearBoth"></div>

 	<?php
  	 require_once 'code/footer.php';
  	 if( isset($_SESSION["mailCita"])){
  	 	echo '<script language="javascript">alert("Su solicitud se ha enviado correctamente, le contestaremos lo mas pronto posible.");</script>'; 
  	 	unset( $_SESSION["mailCita"] ); 
  	 }
  	 ?>
	</div>

	<div class="modal fade vdr_modal-covid" id="modal-container-covid" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog">
			<div class="modal-content vdr_modal-covid-bg">
				<div class="modal-body">
					  
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
						×
					</button>
					<h4 class="modal-title" id="myModalLabel">
						<p id= modal_title>Desinfección gratuita contra el COVID con cualquiera de nuestros servicios</p>
					</h4>
					<div id='mensj' class="text-center">
						<h4 class="vdr_modal-covid-subtitle" id="myModalLabel">
							Higienizamos los coches de nuestros clientes con ozono.
						</h4>

						<p>El ozono es uno de los agentes gaseosos más eficientes a la hora de combatir bacterias y virus como el Covid-19. Es el producto natural con mayor capacidad bactericida de que dispone el hombre y, por tanto, un efectivo destructor de gérmenes, hongos, parásitos y demás organismos nocivos para la salud humana.</p>

						<p>A todos nuestros clientes les realizamos un tratamiento de desinfección e higienización con ozono.</p>

						<button type="button" class="close vdr_modal-covid-close" data-dismiss="modal" aria-hidden="true">Saber más</button>
					</div>
				</div>
			</div>
			
		</div>
		
	</div>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/swiper.min.js"></script>
    <script src="js/validate.min.js"></script>
    <script type="text/javascript">

    (function(){
    	var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        paginationClickable: true,
        spaceBetween: 30,
        centeredSlides: true,
        autoplay: 2500,
        autoplayDisableOnInteraction: false
    	});

    	$('#modal-container-covid').modal('show');
    }());
    </script>
    <?php include_once("code/analyticstracking.php") ?>
  </body>
</html>




