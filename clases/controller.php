<?php
session_start();
require_once 'user_class.php';
require_once 'vehicle_class.php';
require_once 'historial_class.php';
require_once 'newsletter_class.php';

	switch (true){

		case isset($_POST["submit_visita"]):
			move_uploaded_file(	$_FILES['factura']["tmp_name"],"../pdfs/".$_FILES['factura']["name"]);
			header("location: ../historialAdmin.php?id=".$_SESSION['vehicle']);
			break;

		case $_POST['action']==1:
			$array=json_decode($_POST["data"]);
			$newUser = new User(0,trim(strtolower($array[0])),trim(strtolower($array[1])),trim($array[2]),trim($array[3]),trim($array[4]),trim($array[5]),"user");
			$ret=$newUser->register();
			echo json_encode($ret);
			break;

		case $_POST['action']==2:
			$rows= User::getAllUsers();
			echo json_encode($rows);
			break;
			
		case $_POST['action']==3:
			$array=json_decode($_POST["data"]);
			$rows= User::searchUser($array);
			echo json_encode($rows);
			break;

		case $_POST['action']==4:
			$array=json_decode($_POST["data"]);
			$newVehicle = new Vehicle(trim(strtolower($array[0])),$array[5],trim(strtolower($array[1])),trim($array[4]),trim($array[2]),trim($array[3]));
			$ret=$newVehicle->register();
			echo $ret;
			break;

		case $_POST['action']==5:
			$rows= Vehicle::getAllVehicles($_POST['index']);
			echo json_encode($rows);
			break;

		case $_POST['action']==6:
			$array=json_decode($_POST["data"]);
			$newVisit = new Historial(trim(strtolower($array[0])),trim(strtolower($array[1])),trim($array[2]),trim($array[3]),"pdfs/".$array[4]);
			$ret=$newVisit->register();
			echo $ret;
			break;

		case $_POST['action']==7:
			$rows= Historial::getAllHistory($_POST['index']);
			echo (json_encode($rows));
			break;

		case $_POST['action']==8:
			$array=json_decode($_POST["data"]);
			$newUser = new User(0,"","",trim($array[1]),trim(strtolower($array[0])),"","","user");
			$rows= $newUser->login();
			echo $rows;
			break;

		case $_POST['action']==9:
			$rows= Vehicle::getAllVehicles($_SESSION['user'][0]['user_id']);
			echo json_encode($rows);
			break;

		case $_POST['action']==10:
			$rows= Vehicle::getAllVehicles2();
			echo json_encode($rows);
			break;

		case $_POST['action']==11:
			$array=json_decode($_POST["data"]);
			$rows= Vehicle::searchVehicle($array);
			echo json_encode($rows);
			break;

		case $_POST['action']==12:
			$rows= Vehicle::searchVehicle2($_SESSION['vehicle']);
			echo json_encode($rows);
			break;

		case $_POST['action']==13:
			$pass=$_POST["data"];
			if (password_verify($_POST["psw"],$_SESSION['user'][0]['password'])){
				$newUser = new User($_SESSION['user'][0]['user_id'],"","",$pass,"","","","");
				$rows= $newUser->changePass();
				echo true;
			}
			else{
				echo false;
			}
			break;
		case $_POST['action']==14:
			if ($_SESSION['user'][0]['type']=="admin" ){
				$rows= Historial::deleteVisit($_POST['id']);
				echo json_encode($rows);
			}
			else{
				echo false;
			}
			break;

		case $_POST['action']==15:
			if ($_SESSION['user'][0]['type']=="admin" ){
				$rows= Vehicle::deleteVeh($_POST['id']);
				echo true;
			}
			else{
				echo false;
			}
			break;

		case $_POST['action']==16:
			if ($_SESSION['user'][0]['type']=="admin" ){
				$rows= User::deleteUser($_POST['id']);
				echo true;
			}
			else{
				echo false;
			}
			break;

		case $_POST['action']==17:
			$array=json_decode($_POST["data"]);
			$newUser = new Newsletter($array[0]);
			$ret=$newUser->register();
			echo $ret;
			break;
	}

?>
