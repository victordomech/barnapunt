<?php
require_once 'bbdd_class.php';
class Newsletter {
	private $mail; 
	

	// CONSTRUCT
	public function __construct ($mail){
	   $this->mail = $mail;
	}

	// GETTERS
	public function getMail() {
		return $this->mail;
	}
	
	// SETTERS

	public function setMail($mail){
		$this->mail = $mail; 
	}

	public function register(){  //registra un usuario.
	$bbdd = new bbdd();
	$sql1 = "SELECT * from newsletter where email = :mail";
	$row= $bbdd->query($sql1, [':mail' => $this->mail]);
		if($row== false){
			$sql3 = "INSERT INTO newsletter (email) VALUES (:mail)";
			$resultado = $bbdd->query($sql3, [':mail' => $this->mail]);
			return true;
		}
		return false;
	}
}
?>
