<?php
require_once 'bbdd_class.php';
class Historial {
	private $matricula_id; 
	private $servicio; 
	private $descripcion;  
	private $abonado;
	private $facturaPath;

	// CONSTRUCT
	public function __construct ($matricula_id,$servicio,$descripcion,$abonado,$facturaPath){

	   $this->matricula_id = $matricula_id; 
	   $this->servicio = $servicio;
	   $this->descripcion = $descripcion;
	   $this->abonado = $abonado;
	   $this->facturaPath = $facturaPath;
	}

	// GETTERS
	public function getMatricula_id() {
		return $this->matricula_id;
	}
	public function getServicio() {
		return $this->servicio;
	}
	public function getDescripcion() {
		return $this->descripcion;
	}
	public function getAbonado() {
		return $this->abonado;
	}
	public function getFacturaPath() {
		return $this->facturaPath;
	}
	
	// SETTERS
	public function setMatricula_id($id){
		$this->matricula_id = $id; 
	}
	public function setServicio($servicio){
		$this->servicio = $servicio;
	}
	public function setDescripcion($descripcion){
		$this->descripcion = $descripcion;
	}
	public function setAbonado($precio){
		$this->abonado = $precio;
	}
	public function setFacturaPath($facturaPath){
		$this->facturaPath = $facturaPath; 
	}

	// EXTRA GETTERS
	static public function getAllHistory($id){ //llama a la base y devuelve todos los usuarios
		$bbdd = new bbdd();
		$sql2 = "SELECT id,servicio,descripcion,dia,abonado,factura FROM `historial` where matricula_id =:id order by dia desc";
		$rows= $bbdd->query($sql2, [':id' => $id ]);
		return $rows;
	}

	public function register(){  //registra un vehiculo.
	$bbdd = new bbdd();

		$sql2 = "INSERT INTO historial ( matricula_id, servicio, descripcion, dia,abonado ,factura) VALUES (:matricula, :servicio, :descripcion, NOW(),:abonado, :facturaPath)";

		$resultado = $bbdd->query($sql2, [':matricula' => $this->matricula_id,':servicio' => $this->servicio,':descripcion' => $this->descripcion,':abonado' => $this->abonado,':facturaPath' => $this->facturaPath]);
			return true;
	}

	static public function deleteVisit($id){ 
		$bbdd = new bbdd();
		$sql1 = "DELETE FROM `historial` WHERE `historial`.`id` = :id";
		$rows= $bbdd->query($sql1, [':id' => $id]);
		return $rows;
	}
}
?>
