<?php
require_once 'bbdd_class.php';
class User {
	private $id; 
	private $name; 
	private $surname; 
	private $pass;
	private $mail;
	private $telephone; 
	private $postal_c; 
	private $type; 

	// CONSTRUCT
	public function __construct ($id,$name,$surname,$pass,$mail,$telephone, $postal_c, $type){

	   $this->id = $id; 
	   $this->name = $name;
	   $this->surname = $surname;
	   $this->pass = $pass;
	   $this->mail = $mail;
	   $this->telephone = $telephone;
	   $this->postal_c = $postal_c;
	   $this->type = $type;
	}

	// GETTERS
	public function getId() {
		return $this->id;
	}
	public function getName() {
		return $this->name;
	}
	public function getSurname() {
		return $this->surname;
	}
	public function getPass() {
		return $this->pass;
	}
	public function getMail() {
		return $this->mail;
	}
	public function getTelephone() {
		return $this->telephone;
	}
	public function getPostal() {
		return $this->postal_c;
	}
	public function getType() {
		return $this->type;
	}
	
	// SETTERS
	public function setId($id){
		$this->id = $id; 
	}
	public function setName($name){
		$this->name = $name;
	}
	public function setSurname($surname){
		$this->surname = $surname;
	}
	public function setPass($pass){
		$this->pass = $pass; 
	}
	public function setMail($mail){
		$this->mail = $mail; 
	}
	public function setTelephone($telephone){
		$this->telephone = $telephone;
	}
	public function setAdress($postal_c){
		$this->postal_c = $postal_c;
	}
	public function setType($type){
		$this->type = $type;
	}

	// EXTRA GETTERS
	static public function getAllUsers(){ //llama a la base y devuelve todos los usuarios
		$bbdd = new bbdd();
		$sql1 = "SELECT user_id, nombre, apellidos, mail, telefono, codigo_p from usuarios where type='user'; ";
		$rows= $bbdd->query($sql1);
		return $rows;
	}

	static public function searchUser($data){ //llama a la base y devuelve todos los usuarios buscados por nombre,apellido,mail
		$bbdd = new bbdd();
		$sql1 = "SELECT user_id, nombre, apellidos, mail, telefono, codigo_p from usuarios where nombre like '".$data[0]."%' and apellidos like '".$data[1]."%' and mail like '".$data[2]."%' and type='user'";
		$rows= $bbdd->query($sql1);
		return $rows;
	}




	public function register(){  //registra un usuario.
	$bbdd = new bbdd();
	$sql1 = "SELECT * from usuarios where mail = :mail";
	$row= $bbdd->query($sql1, [':mail' => $this->mail]);
	$this->setName(strtolower ( $this->name));
	$this->setSurname(strtolower ( $this->surname));

		if($row== false){
			$this->setPass(password_hash(strtoupper ($this->pass), PASSWORD_DEFAULT));
			$sql2 = "INSERT INTO usuarios ( nombre, apellidos, password, mail, telefono, codigo_p, type) VALUES (:nombre, :apellidos, :password, :mail, :telefono, :codigo_p, :type)";
			$resultado = $bbdd->query($sql2, [':nombre' => $this->name,':apellidos' => $this->surname,':password' => $this->pass,':mail' => $this->mail, ':telefono' => $this->telephone,':codigo_p' => $this->postal_c,':type' => 'user']);


			$row=array('id_user'=>$bbdd->lastId() ,'nombre' => $this->name,'apellidos' => $this->surname,'password' => $this->pass,'mail' => $this->mail, 'telefono' => $this->telephone, 'codigo_p' => $this->postal_c,'type' => 'user');
			return $row;
		}
		return false;
	}

	public function login(){ //hace el login de un usuario.
		unset( $_SESSION["user"]);
		$bbdd = new bbdd();
		$sql1 = "SELECT user_id,nombre,apellidos,password,mail,telefono,codigo_p,type from usuarios where mail = :mail";

		$row= $bbdd->query($sql1, [':mail' => $this->mail]);
		// prepare sql and bind parameter
		if($row!= false){
			if (password_verify($this->pass,$row[0]['password'])){
				$_SESSION['user']=$row;
				return true;
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
	}

	public function changePass(){ //hace el login de un usuario.

		$bbdd = new bbdd();
		$sql1 = "UPDATE `usuarios` SET `password` = :pass WHERE `usuarios`.`user_id` = :id;";
		$this->setPass(password_hash(strtoupper ($this->pass), PASSWORD_DEFAULT));
		$row= $bbdd->query($sql1, [':pass' => $this->pass,':id' => $this->id]);
		$_SESSION['user'][0]['password']=$this->pass;
	}

	static public function deleteUser($id){ 
		$bbdd = new bbdd();
		$sql1 = "DELETE FROM `usuarios` WHERE `usuarios`.`user_id` = :id";
		$rows= $bbdd->query($sql1, [':id' => $id]);
		return $rows;
	}
}
	// $bbdd = new bbdd();
	// 	$options = array("cost" => 10);
	// 	$hash = password_hash('12345', PASSWORD_BCRYPT, $options);
	// 		$sql2 = "INSERT INTO usuarios ( nombre, apellidos, password, mail, telefono, codigo_p, type) VALUES (:nombre, :apellidos, :password, :mail, :telefono, :codigo_p, :type)";
	// 		$resultado = $bbdd->query($sql2, [':nombre' => 'victor',':apellidos' => 'domenech',':password' => $hash,':mail' => 'victor@gmail.com', ':telefono' => '663820728',':codigo_p' => '08110',':type' => 'admin']);

?>
