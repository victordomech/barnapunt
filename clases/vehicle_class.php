<?php
require_once 'bbdd_class.php';
class Vehicle {
	private $matricula_id; 
	private $user_id; 
	private $marca;  
	private $type;
	private $modelo;
	private $year;

	// CONSTRUCT
	public function __construct ($matricula_id,$user_id,$marca,$type,$modelo,$year){

	   $this->matricula_id = $matricula_id; 
	   $this->user_id = $user_id;
	   $this->marca = $marca;
	   $this->type = $type;
	   $this->modelo = $modelo;
	   $this->year = $year;
	}

	// GETTERS
	public function getMatricula_id() {
		return $this->matricula_id;
	}
	public function getUser_id() {
		return $this->user_id;
	}
	public function getMarca() {
		return $this->marca;
	}
	public function getType() {
		return $this->type;
	}
	public function getModelo() {
		return $this->modelo;
	}
	public function getYear() {
		return $this->year;
	}
	
	// SETTERS
	public function setMatricula_id($id){
		$this->matricula_id = $id; 
	}
	public function setUser_id($id){
		$this->user_id = $id;
	}
	public function setMarca($marca){
		$this->marca = $marca;
	}
	public function setType($type){
		$this->type = $type; 
	}
	public function setModelo($modelo){
		$this->modelo = $modelo; 
	}
	public function setYear($year){
		$this->year = $year;
	}

	// EXTRA GETTERS
	static public function getAllVehicles($id){ //llama a la base y devuelve todos los vehiculos
		$bbdd = new bbdd();
		$sql2 = "SELECT matricula_id, user_id, marca, type, modelo, year from vehiculos where user_id = :id;";
		$rows= $bbdd->query($sql2, [':id' => $id ]);
		return $rows;
	}

	static public function getAllVehicles2(){ //llama a la base y devuelve todos los vehiculos
		$bbdd = new bbdd();
		$sql2 = "SELECT matricula_id, user_id, marca, type, modelo, year from vehiculos;";
		$rows= $bbdd->query($sql2);
		return $rows;
	}

	static public function searchVehicle($data){ //llama a la base y devuelve todos los usuarios buscados por nombre,apellido,mail
		$bbdd = new bbdd();
		$sql1 = "SELECT matricula_id, user_id, marca, type, modelo, year from vehiculos where matricula_id like '".$data[0]."%' and marca like '".$data[1]."%'";
		$rows= $bbdd->query($sql1);
		return $rows;
	}

	static public function searchVehicle2($data){ //llama a la base y devuelve todos los usuarios buscados por nombre,apellido,mail
		$bbdd = new bbdd();
		$sql1 = "SELECT matricula_id, user_id, marca, type, modelo, year from vehiculos where matricula_id like '".$data."'";
		$rows= $bbdd->query($sql1);
		return $rows;
	}

	public function register(){  //registra un vehiculo.
	$bbdd = new bbdd();
	$sql1 = "SELECT * from vehiculos where matricula_id = :matricula";
	$row= $bbdd->query($sql1, [':matricula' => $this->matricula_id]);
		if($row== false){
			$sql2 = "INSERT INTO vehiculos ( matricula_id, user_id, marca, type, modelo, year) VALUES (:matricula, :user_id, :marca, :type, :modelo, :year)";

			$resultado = $bbdd->query($sql2, [':matricula' => $this->matricula_id,':user_id' => $this->user_id,':marca' => $this->marca,':type' => $this->type, ':modelo' => $this->modelo,':year' => $this->year]);
			return true;
		}
		return false;
	}

	static public function deleteVeh($id){ 
		$bbdd = new bbdd();
		$sql1 = "DELETE FROM `vehiculos` WHERE `vehiculos`.`matricula_id` = :id";
		$rows= $bbdd->query($sql1, [':id' => $id]);
		return $rows;
	}
}
?>
