<?php // cerramos sesion y volvemos a la pagina donde nos encontrabamos.
session_start();

if(isset($_SESSION['user'])){
	if($_SESSION['user'][0]['type']=='admin'){
		header("location:../adminlog.php");
	}
	else if($_SESSION['user'][0]['type']=='user'){
		header("location:../user.php");
	}
}
else{
	header("location:../index.php");
}
?>