<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Centro de mantenimiento de coches y motos multimarca, venta de recambios,neumáticos y accesorios de coches y motos con la mejor relación calidad-precio" >
	<meta name="keywords" content="turismo, coche, vehiculo, neumáticos, servicios, mecánica,recambios,compra/venta">
	<meta name="application-name" content="BarnaPunt">

    <title>BarnaPunt</title>
    <script type='text/javascript' src="js/jquery.min.js"></script>
	<script type='text/javascript' src='js/unitegallery.min.js'></script>	
	<script type='text/javascript' src='js/ug-theme-tiles.js'></script>
	<link rel='stylesheet' href='css/unite-gallery.css' type='text/css' />
	<link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.min.css" rel="stylesheet">
	<script src="https://use.fontawesome.com/a81c118adb.js"></script>

<body>

  	<div class="container-fluid">
  	 <?php
  	    session_start();
  	 	require_once 'code/header.php';
  	 ?>
	<div id="gallery" style="display:none; margin-top: 4px;">

		
		<img alt="Fachada BarnaPunt"
		     src="img/gallery/BarnaPunt_fachada1.jpg"
		     data-image="img/gallery/BarnaPunt_fachada1.jpg"
		     data-description="Fachada del nuestro taller."
		     style="display:none">

		
		<img alt="Fachada BarnaPunt"
		     src="img/gallery/BarnaPunt_fachada2.jpg"
		     data-image="img/gallery/BarnaPunt_fachada2.jpg"
		     data-description="Fachada del nuestro taller."
		     style="display:none">

		<img alt="Fachada BarnaPunt"
		     src="img/gallery/BarnaPunt_fachada3.jpg"
		     data-image="img/gallery/BarnaPunt_fachada3.jpg"
		     data-description="Fachada del nuestro taller."
		     style="display:none">

		<img alt="Mostrador BarnaPunt"
		     src="img/gallery/BarnaPunt_mostrador1.jpg"
		     data-image="img/gallery/BarnaPunt_mostrador1.jpg"
		     data-description="Mostrador."
		     style="display:none">

		<img alt="Sala de espera"
		     src="img/gallery/BarnaPunt_sala_modelo1.jpg"
		     data-image="img/gallery/BarnaPunt_sala_modelo1.jpg"
		     data-description="Sala de espera."
		     style="display:none">

		<img alt="Sala de espera"
		     src="img/gallery/BarnaPunt_sala_modelo2.jpg"
		     data-image="img/gallery/BarnaPunt_sala_modelo2.jpg"
		     data-description="Tomate un cafe mientras esperas."
		     style="display:none">

		<img alt="Sala de espera"
		     src="img/gallery/BarnaPunt_sala_modelo3.jpg"
		     data-image="img/gallery/BarnaPunt_sala_modelo3.jpg"
		     data-description="Espera viendo la tele, pidenos el canal que quieras."
		     style="display:none">

		<img alt="Sala de espera"
		     src="img/gallery/BarnaPunt_sala_modelos1.jpg"
		     data-image="img/gallery/BarnaPunt_sala_modelos1.jpg"
		     data-description="Espera mientras charlas con un amigo."
		     style="display:none">

		<img alt="Sala de espera"
		     src="img/gallery/BarnaPunt_sala1.jpg"
		     data-image="img/gallery/BarnaPunt_sala1.jpg"
		     data-description="Sala de espera."
		     style="display:none">

		<img alt="Sala de espera"
		     src="img/gallery/BarnaPunt_sala2.jpg"
		     data-image="img/gallery/BarnaPunt_sala2.jpg"
		     data-description="Sala de espera."
		     style="display:none">

		<img alt="Taller"
		     src="img/gallery/BarnaPunt_taller1.jpg"
		     data-image="img/gallery/BarnaPunt_taller1.jpg"
		     data-description="Taller."
		     style="display:none">

		<img alt="Taller"
		     src="img/gallery/BarnaPunt_taller2.jpg"
		     data-image="img/gallery/BarnaPunt_taller2.jpg"
		     data-description="Taller"
		     style="display:none">

		<img alt="Taller"
		     src="img/gallery/BarnaPunt_taller3.jpg"
		     data-image="img/gallery/BarnaPunt_taller3.jpg"
		     data-description="Taller."
		     style="display:none">

		<img alt="Taller"
		     src="img/gallery/BarnaPunt_taller4.jpg"
		     data-image="img/gallery/BarnaPunt_taller4.jpg"
		     data-description="Taller"
		     style="display:none">

		<img alt="Taller"
		     src="img/gallery/BarnaPunt_taller5.jpg"
		     data-image="img/gallery/BarnaPunt_taller5.jpg"
		     data-description="Taller."
		     style="display:none">

		<img alt="Taller"
		     src="img/gallery/BarnaPunt_taller6.jpg"
		     data-image="img/gallery/BarnaPunt_taller6.jpg"
		     data-description="Taller."
		     style="display:none">

		<img alt="Taller"
		     src="img/gallery/BarnaPunt_taller7.jpg"
		     data-image="img/gallery/BarnaPunt_taller7.jpg"
		     data-description="Taller"
		     style="display:none">

		<img alt="Tienda"
		     src="img/gallery/BarnaPunt_tienda1.jpg"
		     data-image="img/gallery/BarnaPunt_tienda1.jpg"
		     data-description="Tienda."
		     style="display:none">

		<img alt="Tienda"
		     src="img/gallery/BarnaPunt_tienda2.jpg"
		     data-image="img/gallery/BarnaPunt_tienda2.jpg"
		     data-description="Tienda"
		     style="display:none">

		<img alt="Tienda"
		     src="img/gallery/BarnaPunt_tienda3.jpg"
		     data-image="img/gallery/BarnaPunt_tienda3.jpg"
		     data-description="Tienda."
		     style="display:none">

		<img alt="Tienda"
		     src="img/gallery/BarnaPunt_tienda4.jpg"
		     data-image="img/gallery/BarnaPunt_tienda4.jpg"
		     data-description="Tienda."
		     style="display:none">

		<img alt="Nuestros mecanicos trabajando"
		     src="img/gallery/BarnaPunt_trabajando1.jpg"
		     data-image="img/gallery/BarnaPunt_trabajando1.jpg"
		     data-description="Nuestros mecanicos trabajando."
		     style="display:none">

		<img alt="Nuestros mecanicos trabajando"
		     src="img/gallery/BarnaPunt_trabajando2.jpg"
		     data-image="img/gallery/BarnaPunt_trabajando2.jpg"
		     data-description="Nuestros mecanicos trabajando."
		     style="display:none">
		<img alt="Clientes en tienda"
		     src="img/gallery/BarnaPunt_tienda_modelo1.jpg"
		     data-image="img/gallery/BarnaPunt_tienda_modelo1.jpg"
		     data-description="Clientes en tienda."
		     style="display:none">
		<img alt="Clientes en tienda"
		     src="img/gallery/BarnaPunt_tienda_modelo2.jpg"
		     data-image="img/gallery/BarnaPunt_tienda_modelo2.jpg"
		     data-description="Clientes en tienda."
		     style="display:none">
			 
	</div>
	</main>
	<?php
  	 require_once 'code/footer.php';
  	 ?>
</div>
	
	<script type="text/javascript">

		jQuery(document).ready(function(){

			jQuery("#gallery").unitegallery();

		});
		
	</script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/validate.min.js"></script>
	<?php include_once("code/analyticstracking.php") ?>


</body>
</html>
