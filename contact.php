<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Centro de mantenimiento de coches y motos multimarca, venta de recambios,neumáticos y accesorios de coches y motos con la mejor relación calidad-precio, barnapuncar" >
 	<meta name="keywords" content="turismo, coche, vehiculo, neumáticos, servicios, mecánica,recambios,compra/venta,Barnapuntcar,BarnaPunt,barnapunt">
  	<meta name="application-name" content="Barnapuntcar">

    <title>BarnaPunt</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.min.css" rel="stylesheet">

    <script src="https://use.fontawesome.com/a81c118adb.js"></script>

  </head>
  	<body>
	  	<div class="container-fluid">

		 	 <?php
		 	    session_start();
		  	 	require_once 'code/header.php';
		  	 ?>

			<main>
				<div class="modal fade" id="modal-container-589562" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog" id="modal">
						<div class="modal-content">
							<div class="modal-body">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									×
								</button>
								<h4 class="modal-title" id="myModalLabel">
									<p>Politica de privacidad</p>
								</h4>
								<p>Política de privacidad
									En cumplimiento de la Ley Orgánica 15/1999 de 13 de diciembre, de protección de datos de carácter personal, le informamos que sus datos serán incorporados a un fichero de datos titularidad de <b>BARNAPUNT-CAR 2017, S.L</b>, cuya finalidad es la gestión de la cita, gestionar facturas y atender sus consultas, y en alguna ocasión, de promociones comerciales que puedan ser de su interés.<br>

									Le informamos de que usted podrá ejercer sus derechos de acceso, rectificación, cancelación y oposición mediante comunicación escrita a <b>BARNAPUNT-CAR 2017, S.L</b> (Carrer Major, 12, 08759 Vallirana (Barcelona)), incluyendo la referencia “Protección de Datos” y acompañando una fotocopia de su DNI o documento identificativo equivalente. También puede dirigirse personalmente al establecimiento.
								</p>
								<div class="modal-footer" >
									<button type="button" class="btn btn-default" data-dismiss="modal" id="exitModal">
										Cerrar
									</button> 
								</div>
							</div>
						</div>
					</div>
				</div>
				<div Id="clearBoth"></div>
				<div class="row row_padding grisClarito">
					<div class="col-md-6 colIzPading marginTop">
						<article id="InfoLeft" class="text-center">
							<h3>ATENCIÓN AL CLIENTE <br> 935 17 70 21</h3>
							<h4>HORARIO DE LUNES A VIERNES: DE 9H A 13:30H y 15:30H A 20:00H</h4>
							<h4>HORARIO DE SÁBADOS: DE 9H A 14:00H</h4>
						</article>
					</div>
					<div class="col-md-12" id="map1">
						<div class="marginTop">
							<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d11973.24216473215!2d1.9447334!3d41.3890619!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x25f4765fd002a3b3!2sBarnaPuntCar!5e0!3m2!1ses!2ses!4v1501534500827" width="100%" height="400" frameborder="0" style="border:2px solid #f1dd38" allowfullscreen></iframe>
						</div>
					</div>
					<div class="col-md-6 colDerPading marginTopNone">
						<form class="form-horizontal" id="contact" sin_margin method="post" action="code/contactMail.php" onsubmit="return validaContact()">
							<div class="form-group">
								<label class="control-label col-md-4 colIzPading"><span class="fa fa-user colIzPading" area-hidden="true"></span> Nombre:</label>
								<div class="col-md-8">
									<input type="text" class="form-control" placeholder="Nombre" name="name2" id="name2">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4 colIzPading"><span class="glyphicon fa fa-user colIzPading" area-hidden="true"></span> Apellido:</label>
								<div class="col-md-8">
									<input type="text" class="form-control" placeholder="Apellido" name="surname2" id="surname2">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4 colIzPading"><span class="fa fa-phone  colIzPading" area-hidden="true"></span> Telefono:</label>
								<div class="col-md-8">
									<input type="tel" class="form-control" placeholder="Telefono" name="telephone2" id="telephone2">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4 colIzPading"><span class="fa fa-at colIzPading" area-hidden="true"></span> Correo electronico:</label>
								<div class="col-md-8">
									<input type="text" class="form-control" placeholder="Email" name="email2" id="email2">
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-4 colIzPading"><span class="fa fa-commenting colIzPading" area-hidden="true"></span> Comentario:</label>
								<div class="col-md-8">
									<textarea rows="3" class="form-control" placeholder="¿En que te podemos ayudar?" name="commentary2" id="commentary2"></textarea>
								</div>
							</div>
							<div class="form-group text-center">
									<label class="checkbox-inline" id="checkboxLabel">
											<input type="checkbox" value="agree" name="conditions" id="conditions">  Accepto  
									</label><a href='#modal-container-589562'  class='navegacion' data-toggle='modal'> Terminos y condiciones.</a>
							</div>
						    <div class="form-group text-center">
								<button type="submit" value="Enviar" class="btn bot">
									<p>Enviar solicitud</p>
								</button>
						    </div>
						</form>
					</div>
					<div class="col-md-12" id="map2">
						<div>
							<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d11973.24216473215!2d1.9447334!3d41.3890619!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x25f4765fd002a3b3!2sBarnaPuntCar!5e0!3m2!1ses!2ses!4v1501534500827" width="100%" height="400" frameborder="0" style="border:2px solid #f1dd38" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</main>

	<?php
  	 require_once 'code/footer.php';
  	 if( isset($_SESSION["mailContact"])){
  	 	echo '<script language="javascript">alert("El mensaje se ha enviado correctamente, le contestaremos lo mas pronto posible.");</script>'; 
  	 	unset( $_SESSION["mailContact"] ); 
  	 }
  	 ?>

		</div>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/validate.min.js"></script>
    <?php include_once("code/analyticstracking.php") ?>
  	</body>
</html>