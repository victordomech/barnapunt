<div id='showUsers'>
       <h3 class="text-center titulos T_mar" >INFORMACION USUARIOS</h3>
    <div class="row ">
        <div class="col-md-12">
        <form class="form-horizontal" id="searchUsers" sin_margin method="post" action="#">
            <div class="form-group col-md-4">
              <label class="control-label col-md-4 colIzPading"><span class="fa fa-user colIzPading" area-hidden="true"></span> Nombre:</label>
              <div class="col-md-8">
                <input type="text" class="form-control"  onkeyup="searchUser()" placeholder="Nombre" name="name2" id="name2">
              </div>
            </div>
            <div class="form-group col-md-4">
              <label class="control-label col-md-4 colIzPading"><span class="fa fa-user colIzPading" area-hidden="true"></span> Apellido:</label>
              <div class="col-md-8">
                <input type="text" class="form-control" onkeyup="searchUser()" placeholder="Apellido" name="surname4" id="surname4">
              </div>
            </div>
            <div class="form-group col-md-4">
              <label class="control-label col-md-4 colIzPading"><span class="fa fa-at colIzPading" area-hidden="true"></span> Email:</label>
              <div class="col-md-8">
                <input type="text" class="form-control" onkeyup="searchUser()" placeholder="Email" name="email4" id="email4">
              </div>
            </div>
        </form>
          <table class="table table-hover">
              <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Apellido</th>
                  <th>Mail</th>
                  <th>Telefono</th>
                  <th>Codigo Postal</th>
                  <th>Acciones</th>
                  </tr>
              </thead>
              <tbody id= 'tableBody'>
              </tbody>
          </table>
        </div>
      </div>
 </div>
