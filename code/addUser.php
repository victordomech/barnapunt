<div id='showAddUsers' hidden>
  <h3 class="text-center titulos">AÑADIR USUARIO</h3>
    <div class="row">
        <div class="col-md-12">
        	<form class="form-horizontal" id="addUser"  method="post" action="#">
						<div class="form-group">
							<label class="control-label col-md-4 colIzPading"><span class="fa fa-user colIzPading" area-hidden="true"></span> Nombre*:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" placeholder="Nombre" name="name3" id="name3">
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-4 colIzPading"><span class="glyphicon fa fa-user colIzPading" area-hidden="true"></span> Apellidos:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" placeholder="Apellido" name="surname3" id="surname3">
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-4 colIzPading"><span class="fa fa-unlock-alt colIzPading" area-hidden="true"></span> Contraseña*:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" placeholder="Matricula" name="password3" id="password3">
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-4 colIzPading"><span class="fa fa-at colIzPading" area-hidden="true"></span> Correo electronico*:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" placeholder="Email" name="email3" id="email3">
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-4 colIzPading"><span class="fa fa-phone  colIzPading" area-hidden="true"></span> Telefono:</label>
							<div class="col-md-8">
								<input type="tel" class="form-control" placeholder="Telefono" name="telephone3" id="telephone3">
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-4 colIzPading"><span class="glyphicon fa fa-user colIzPading" area-hidden="true"></span> Codigo Postal*:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" placeholder="08759" name="postal_c3" id="postal_c3">
							</div>
						</div>
						
					    <div class="form-group text-center">
						<button type="button" onclick="register()"  value="Enviar" class="btn bot">
								<p>Añadir usuario</p>
							</button>
					    </div>
					</form>
					<div id='mens'></div>
				  </div>
			</div>
		</div>
