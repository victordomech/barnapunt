<div class="modal fade" id="modal-container-223823" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
					  
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					×
				</button>
				<h4 class="modal-title" id="myModalLabel">
					<p id= modal_title>Cambiar contraseña</p>
				</h4>

				<form class="form-horizontal sin_margin" id="changePass" method="post" action="#">
						
					<div class="form-group">
						<label class="control-label col-md-4 colIzPading"><span class="fa fa-unlock-alt colIzPading" area-hidden="true"></span> Contraseña actual:</label>
						<div class="col-md-6">
							
							<input type="password" class="form-control"  name="actualPass" id="actualPass">

						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-4 colIzPading"><span class="fa fa-unlock-alt colIzPading" area-hidden="true"></span> Nueva contraseña:</label>
						<div class="col-md-6">
							
							<input type="password" class="form-control"  name="newPass" id="newPass">

						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-4 colIzPading"><span class="fa fa-unlock-alt colIzPading" area-hidden="true"></span> Repita la nueva contraseña:</label>
						<div class="col-md-6">
							
							<input type="password" class="form-control"  name="newPassRepeat" id="newPassRepeat">

						</div>
					</div>
					<button id= "botLog" type="button" value="Enviar" class="btn bot" onclick="changePasss()" >
						<p>Aceptar</p>
					</button>
				</form>
				<div id='mensj' class="text-center"></div>
			</div>
		</div>
	</div>
</div>