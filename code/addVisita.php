<div id='showAddHist' hidden>
  <h3 class="text-center titulos">AÑADIR VISITA</h3>
    <div class="row colDerPading">
        <div class="col-md-12 ">
        	<form class="form-horizontal" id="addVisit"  method="post" onsubmit="return registerVisit()" action="clases/controller.php" enctype="multipart/form-data">
        		<div class="form-group">
					<label class="control-label col-md-4 colIzPading">
					<span class="fa fa-wrench" colIzPading area-hidden="true"></span> Selecciona el tipo servicio</label>
					<select name="services" id="services" class="col-md-6 colIzPading">
						<option value="seleccionar">seleccionar</option>
						<option value="mantenimiento">Mantenimiento</option>
						<option value="mecanica">Mecánica</option>
						<option value="electricidad">Electricidad</option>
						<option value="diagnosis">Diagnosis</option>
						<option value="climatización">Climatización</option>
						<option value="frenos">Frenos</option>
						<option value="neumáticos">Neumáticos</option>
						<option value="multimedia">Multimedia</option>
					</select>
				</div>

				<div class="form-group">
					<label class="control-label col-md-4 colIzPading"><span class="fa fa fa-pencil colIzPading" area-hidden="true"></span>Trabajos realizados:</label>
					<div class="col-md-8">
						<textarea rows="3" class="form-control" name="commentary3" id="commentary3"></textarea>
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-4 colIzPading"><span class="fa fa-eur colIzPading" area-hidden="true"></span> Abonado:</label>
					<div class="col-md-8">
						<input type="text" class="form-control" placeholder="Abonado en la factura" name="abonado" id="abonado">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-4 colIzPading"><span class="fa fa-file-pdf-o colIzPading" area-hidden="true"></span> Factura PDF:</label>
					<div class="col-md-8">
						<input type="file" id="factura" name="factura" accept=".pdf" required/>
					</div>
				</div>

				<div class="form-group text-center">
					<button type="submit" value="Enviar" class="btn bot" name="submit_visita">
						<p>Añadir Visita</p>
					</button>
					<button type="button" id='cancel' value="cancel" class="btn">
						<p>Cancelar</p>
					</button>
				</div>
			</form>
		</div>
	</div>
</div>