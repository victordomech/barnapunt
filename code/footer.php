<footer>
		<div class="row">
			<div class="col-md-12">
				<p id="fotP">BARNAPUNT SERVICES</p>
			</div>
		</div>
		<div class="row gris row_padding">
			<div class="col-md-4 col-sm-6 col-xs-12 fot row_margin" >
				<article class="centrarH center">
					<p>
						Copyright © <b>BarnaPuntCar Services</b> 2017<br>
						<b>Taller mecanico y tienda de neumáticos, recambios y accesorios.</b><br>
						Carrer Major, 12 ( Vallirana ) <br>
						08759 <b>BARCELONA</b> · Tel.: <b>935 17 70 21</b>
					</p>
				</article>
			</div>
			<div class="col-md-4 col-sm-6 fot col-xs-12 row_margin">
				<article class="centrarH text-center">
					<p id="Newsletter">Suscríbete a nuestro newsletter</p>
					<p >Y te mantendremos informado de las novedades...</p>

					<div id="MailBox ">
						<p>INTRODUCE TU E-MAIL</p>
						<div>
							<input type="email" class="form-control" id="inputEmail"  placeholder="Email">
						  	<button type="button" id="botEmail" onclick='registerNews()'>
								<p>Suscríbete</p>
							</button>
						</div>
					</div>
				</article>
			</div>
			<div class="col-md-4 col-sm-12 col-xs-12 row_margin">
				<article class="centrarH text-center">
		           	<ul class="social-icons icon-circle list-unstyled list-inline icon-rotate">
		            	<li> <a href="https://www.instagram.com/barnapuntcar/"target="_blank"><i class="fa fa-instagram sociall"></i></a></li>
		              	<li> <a href="https://www.facebook.com/Barnapuntcar/" target="_blank"><i class="fa fa-facebook sociall"></i></a></li>
		              	<li> <a href="https://www.youtube.com/channel/UCdYjEPuqV6v1L7m2-hhPmRQ" target="_blank"><i class="fa fa-youtube sociall"></i></a></li>
		              	<!-- <li> <a href="#" ><i class="fa fa-twitter sociall"></i></a></li> -->
		            </ul>
      			</article>
			</div>
		</div>
</footer>