<header>
	<?php 
		if(isset($_SESSION['user'])){
			require_once 'code/changePass.php';
		}
	?>
			<div class="modal fade" id="modal-container-223825" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-body">
							  
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
								×
							</button>
							<h4 class="modal-title" id="myModalLabel">
								<p id= modal_title>Login</p>
							</h4>

							<form class="form-horizontal sin_margin" id="login" method="post" action="#">

								<div class="form-group">
									<label class="control-label col-md-4 colIzPading"><span class="fa fa-user colIzPading" area-hidden="true"></span> Usuario:</label>
									<div class="col-md-6">
										<input type="text" class="form-control" placeholder="Usuario" name="userlogin"
										id="userlogin">
									</div>
								</div>


								<div class="form-group">
									<label class="control-label col-md-4 colIzPading"><span class="fa fa-unlock-alt colIzPading" area-hidden="true"></span> Contraseña:</label>
									<div class="col-md-6">
										<input type="password" class="form-control" placeholder="Contraseña" name="passwordlogin" id="passwordlogin">

									</div>
								</div>
									<button id= "botLog" type="button" value="Enviar" class="btn bot" onclick="login()" >
										<p>Aceptar</p>
									</button>
							</form>
							<div id='mensj' class="text-center"></div>
						</div>
					</div>
					
				</div>
				
			</div>
  	<div class="modal fade" id="modal-container-589569" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog" id="modal">
					<div class="modal-content">
						<div class="modal-body">
						<div id='politica' hidden>
							<button type="button " class=" exit exitPolitic exitPolitica"  aria-hidden="true">
								×
							</button>
							<h4 class="modal-title" id="myModalLabel">
								<p>Politica de privacidad</p>
							</h4>
							<p>Política de privacidad
								En cumplimiento de la Ley Orgánica 15/1999 de 13 de diciembre, de protección de datos de carácter personal, le informamos que sus datos serán incorporados a un fichero de datos titularidad de <b>BARNAPUNT-CAR 2017, S.L</b>, cuya finalidad es la gestión de la cita, gestionar facturas y atender sus consultas, y en alguna ocasión, de promociones comerciales que puedan ser de su interés.<br>

								Le informamos de que usted podrá ejercer sus derechos de acceso, rectificación, cancelación y oposición mediante comunicación escrita a <b>BARNAPUNT-CAR 2017, S.L</b> (Carrer Major, 12, 08759 Vallirana (Barcelona)), incluyendo la referencia “Protección de Datos” y acompañando una fotocopia de su DNI o documento identificativo equivalente. También puede dirigirse personalmente al establecimiento.
								</p>
							<div class="modal-footer center" >
							 
							<button type="button" value="Enviar" class="btn bot exitPolitica">
								<p>Volver al formulario</p>
							</button>
							</div>
						</div>
							<div id="cita" show >
							<button type="button" class="close exit" data-dismiss="modal" aria-hidden="true">
								×
							</button>
							<h4 class="modal-title" id="myModalLabel">
								<p>Solicitar cita</p>
							</h4>

							
							<?php
							require_once('code/recaptchalib.php');
							?>




							<form class="form-horizontal sin_margin" id="citar" method="post" action="code/citaMail.php" onsubmit="return validaForm()">


								<div class="form-group">
									<label class="control-label col-md-4 colIzPading"><span class="fa fa-user colIzPading" area-hidden="true"></span> Nombre:</label>
									<div class="col-md-6">
										<input type="text" class="form-control" placeholder="Nombre" name="name"
										id="name">
									</div>
								</div>


								<div class="form-group">
									<label class="control-label col-md-4 colIzPading"><span class="fa fa-user colIzPading" area-hidden="true"></span> Apellido:</label>
									<div class="col-md-6">
										<input type="text" class="form-control" placeholder="Apellido" name="surname" id="surname">

									</div>
								</div>


								<div class="form-group">
									<label class="control-label col-md-4 colIzPading"><span class="fa fa-phone colIzPading" area-hidden="true"></span> Telefono:</label>
									<div class="col-md-6">
										<input type="tel" class="form-control" placeholder="Telefono" name="telephone" id="telephone">
									</div>
								</div>


								<div class="form-group">
									<label class="control-label col-md-4 colIzPading"><span class="fa fa-at colIzPading" area-hidden="true"></span> Correo electronico:</label>
									<div class="col-md-6">
										<input type="text" class="form-control" placeholder="Email" name="email" id="email">
									</div>
								</div>


								<div class="form-group">
									<label class="control-label col-md-4 colIzPading">
										<span class="fa fa-wrench colIzPading"  area-hidden="true"></span> Seleccione el tipo de servicio:</label>
									<select name="services" id="services" class="col-md-6 colIzPading">
										<option value="seleccionar">seleccionar</option>
									  	<option value="mantenimiento">Mantenimiento</option>
									  	<option value="mecanica">Mecanica</option>
									  	<option value="electricidad">Electricidad</option>
									  	<option value="diagnosis">Diagnosis</option>
									    <option value="climatización">Climatización</option>
									   	<option value="frenos">Frenos</option>
									   	<option value="neumáticos">Neumáticos</option>
									   	<option value="multimedia">Multimedia</option>
									   	</select>
								</div>


								<div class="form-group">
									<label class="control-label col-md-4 colIzPading"><span class="fa fa-commenting colIzPading" area-hidden="true"></span> Comentario:</label>
									<div class="col-md-6">
										<textarea rows="3" class="form-control" placeholder=" Expliquenos mas concretamente que servicios necesita" name="commentary" id="commentary"></textarea>
									</div>
								</div>


								<div class="form-group text-center">
										<label class="checkbox-inline" id="checkboxLabel">
										<input type="checkbox" value="agree" name="conditions" id="conditions">  Accepto  
										</label><a id="politicaButton"> Terminos y condiciones.</a>
								</div>
								<p id="errorf"></p>	

							<div class="modal-footer">
							 
							<button type="button" class="btn btn-default" data-dismiss="modal" id="exitModal">
								Cerrar
							</button> 

							<div class="g-recaptcha" data-sitekey="6LdKBkgUAAAAAHRk-epwpMY4Ry0VomGEbYNV1wzY" name="recaptcha" id="recaptcha"></div>

							<button type="submit" value="Enviar" class="btn bot">
								<p>Enviar solicitud</p>
							</button>


							</div>
						</div>
						</form>
					</div>
				</div>
					
			</div>
				
		</div>
	    <div class="row" >
			<div class="col-md-12">
				<ul id="sub_menu">
					<li class="sub_menu_iz">
						<a href="#"><span class="nav_color2">ES </span></a>
					</li>
					 
					<!-- <li class="sub_menu_iz">
						<a href="#"><span class="nav_color2">  CAT  </span></a>
					</li> -->
					<!-- <li id="sub_menu_center">
						<a href="#"><span class="nav_color2">TIENDA</span></a>
					</li> -->
					<li id="sub_menu_der" class='dropdown'>
					<?php 
						$str;
						if(isset($_SESSION['user'])){

							$str="
							 <a href='#' class='dropdown-toggle nav_color2' data-toggle='dropdown' >Bienvenido ".strtoupper($_SESSION['user'][0]['nombre'])."<strong class='caret'></strong></a>
							<ul class='dropdown-menu'>";
									if($_SESSION['user'][0]['type']=='admin'){
										$str.="<li>
									<a href='adminlog.php' class='nav_color2'>Panel administrador</a>
									</li>";
									}
									else{
										$str.="<li>
									<a href='user.php' class='nav_color2'>Mis vehiculos</a></li>
									<li>
									<a href='#modal-container-223823'  class='navegacion' data-toggle='modal'>Cambiar contraseña</a>
								</li>";
									}
									$str.="
								<li class='divider'>
								</li>
								<li>
									<a href='clases/sessionOut.php' class='nav_color2'>Cerrar sesion</a>
								</li>
							</ul>";
						}
						else{
							$str="<a href='#modal-container-223825'  class='navegacion' data-toggle='modal'><span class='nav_color2'> AREA CLIENTES </span></a>";
						}
						echo $str;
					?>
					</li> 
				</ul>
			</div>
		</div>
		<div Id="clearBoth"></div>

		<div class="row">
			<div class="col-md-12">
				<nav class="navbar navbar-default" role="navigation" id="menux" >
					<div>
						<div class="navbar-header">
							 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
								<span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar sanwich"></span>
							</button> 
							<div id="logoDiv"><a href="index.php">
									<img src="img/logo_barnapunt.svg"  id="logo">
									<img src="img/logo_barnapunt2.svg" id="logo2">
							</a></div>
						</div>
						
						<div class="collapse navbar-collapse borderBox" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav navbar-right altura">
								<li>
									<a href="promociones.php" class="navegacion"><span class="nav_color">Promociones</span></a>
								</li>
								<li>
									<a href="services.php" class="navegacion"><span class="nav_color">Servicios</span></a>
								</li>
								<li>
									<a href="contact.php" class="navegacion"><span class="nav_color">Contacto</span></a>
								</li>
								 <!-- <li>
									<a href="#modal-container-589569"  class="navegacion" data-toggle="modal"><span class="nav_color">Solicitar Cita</span></a>
								</li>  -->
								<li>
									<a href="gallery.php" class="navegacion"><span class="nav_color">Galeria</span></a>
								</li>
							</ul>
						</div>
					</div>
				</nav>
			</div>
		</div>
</header>