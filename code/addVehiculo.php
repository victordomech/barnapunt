<div id='showAddVeh' hidden>
  <h3 class="text-center titulos" id="titAdd">AÑADIR VEHICULO</h3>
    <div class="row colDerPading">
        <div class="col-md-12 ">
        	<form class="form-horizontal" id="addVeh"  method="post" action="#">
						<div class="form-group">
							<label class="control-label col-md-4 colIzPading"><span class="fa fa-id-card colIzPading" area-hidden="true"></span> Matricula*:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" placeholder="Matricula" name="matricula5" id="matricula5">
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-4 colIzPading"><span class="fa fa-tags colIzPading" area-hidden="true"></span> Marca*:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" placeholder="Marca vehiculo" name="marca5" id="marca5">
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-4 colIzPading">
								<span class="fa fa-car" colIzPading area-hidden="true"></span> Selecciona el tipo de vehiculo:</label>
								<select name="tipoVehiculo" id="tipoVehiculo" class="col-md-6 colIzPading">
										<option value="seleccionar">seleccionar</option>
									  	<option value="turismo">Turismo</option>
									  	<option value="motocicleta">Motocicleta</option>
									  	<option value="ciclomotor">Ciclomotor</option>
									  	<option value="quad">Quad</option>
									    <option value="furngoneta">Furgón/Furgoneta MMA 3.500 kg </option>
									   	<option value="tresruedas">Automóvil de tres ruedas</option>
								</select>
						</div>
						<div class="form-group">
							<label class="control-label col-md-4 colIzPading"><span class="fa fa-tags colIzPading" area-hidden="true"></span> Modelo:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" placeholder="Modelo vehiculo" name="modelo5" id="modelo5">
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-4 colIzPading"><span class="fa fa-calendar-plus-o colIzPading" area-hidden="true"></span> Año:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" placeholder="2005" name="año5" id="año5">
							</div>
						</div>
						
					    <div class="form-group text-center">
							<button type="button" onclick="registerVehiculo()"  value="Enviar" class="btn bot">
								<p>Añadir vehiculo</p>
							</button>
							<button type="button" id='cancel' value="cancel" class="btn">
								<p>Cancelar</p>
							</button>
					    </div>
					</form>
					<div id='mens'></div>
				  </div>
			</div>
		</div>