<div id='showCars' hidden>
      <h3 class="text-center titulos T_mar" >INFORMACION USUARIOS</h3>
    <div class="row ">
        <div class="col-md-12">
        <form class="form-horizontal" id="searchVehicles" sin_margin method="post" action="#">
            <div class="form-group col-md-6">
              <label class="control-label col-md-4 colIzPading"><span class="fa fa-user colIzPading" area-hidden="true"></span> Matricula:</label>
              <div class="col-md-8">
                <input type="text" class="form-control"  onkeyup="searchVehicles()" placeholder="Matricula" name="matricula" id="Matricula">
              </div>
            </div>
            <div class="form-group col-md-6">
              <label class="control-label col-md-4 colIzPading"><span class="fa fa-user colIzPading" area-hidden="true"></span> Marca:</label>
              <div class="col-md-8">
                <input type="text" class="form-control" onkeyup="searchVehicles()" placeholder="Marca" name="Marca" id="Marca">
              </div>
            </div>
        </form>
          <table class="table table-hover">
              <thead>
                <tr>
                  <th>Matricula</th>
                  <th>Marca</th>
                  <th>Tipo</th>
                  <th>Modelo</th>
                  <th>Año</th>
                  </tr>
              </thead>
              <tbody id= 'tableBody2'>
              </tbody>
          </table>
        </div>
      </div>
 </div>
