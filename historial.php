<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Centro de mantenimiento de coches y motos multimarca, venta de recambios,neumáticos y accesorios de coches y motos con la mejor relación calidad-precio, barnapuncar" >
   <meta name="keywords" content="turismo, coche, vehiculo, neumáticos, servicios, mecánica,recambios,compra/venta,Barnapuntcar,BarnaPunt,barnapunt">
   <meta name="application-name" content="Barnapuntcar">

    <title>BarnaPunt</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.min.css" rel="stylesheet">
    <link type="text/css" href="css/jquery.mmenu.all.css" rel="stylesheet" />


    <script src="js/jquery.min.js"></script>
    <script src="https://use.fontawesome.com/a81c118adb.js"></script>
  </head>

  <body>
    <div class="container-fluid">
     <?php
     session_start();
      if(!isset($_SESSION['user'])){
          header("location: index.php");
        }
     $_SESSION['vehicle']=$_GET["id"];
     require_once 'code/header.php';
     ?> 
  <div Id="clearBoth"></div>
    <div class="container1250">
      <h3 class="text-center titulos row_padding52"><a href="user.php"><i class="fa fa-arrow-left" aria-hidden="true" id="lArrow" ></i></a> HISTORIAL DE SU VEHICULO </h3>
    </div>
    <div class="container1250 margin_bottom">
      <div class="row row_padding">

      <div class="col-md-12" id='showHistorial'></div>

      </div>
    </div>
  <div Id="clearBoth"></div>

  <?php
     require_once 'code/footer.php';
     ?>
  </div>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/historialUser.js"></script>
    <script src="js/validate.min.js"></script>
    <?php include_once("code/analyticstracking.php") ?>

  </body>
</html>