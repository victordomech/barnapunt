<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="description" content="Centro de mantenimiento de coches y motos multimarca, venta de recambios,neumáticos y accesorios de coches y motos con la mejor relación calidad-precio, barnapuncar" >
  <meta name="keywords" content="turismo, coche, vehiculo, neumáticos, servicios, mecánica,recambios,compra/venta,Barnapuntcar,BarnaPunt,barnapunt">
  <meta name="application-name" content="Barnapuntcar">

    <title>BarnaPunt</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.min.css" rel="stylesheet">

    <script src="https://use.fontawesome.com/a81c118adb.js"></script>

  </head>
  <body>
    <div class="container-fluid">
     <?php
        session_start();
        require_once 'code/header.php';
     ?>
 <main>
 <div Id="clearBoth"></div>
    
    <div class="row row_padding grisClarito" >
    <div class="container1250">
      <div class="col-md-12">
        <h3 class="text-center titulos colorfont_gris" >EN PROMOCIÓN</h3>
        <div class="row row_margin oferta">
          <div class="col-md-12 gris" id="aceite">
              <div class="col-md-8 captionOferta">
                  <h3 class="colorfont_amarillo center">
                    CAMBIO DE ACEITE
                  </h3>
                  <p class="colorfont_blanco p400">
                  Realiza el cambio de aceite de tu vehículo al mejor precio: 10W40 + filtro de aceite por solo 69,90 €. Trabajamos con marcas líderes en lubricantes.<br><br>

                    - La promoción incluye aceite 10W40, revisión de niveles, presión de neumáticos y frenos.<br>
                    - La promoción incluye filtro de aceite.<br>
                    - Para otros aceites, consultar precio con promoción.<br>
                    - Incluye mano de obra e impuestos.<br>
                    - <span style="font-size: 12px; font-style: italic; color: #f1dd38;">Incluye un maximo de 5L de aceite*</span>
                  </p>
                </div>
              <div class="col-md-4 promocionImg">
                  <img alt="img cambio aceite" src="img/cambio_aceite.jpg">
              </div>
            </div>
          </div>

          <div class="row row_margin oferta">
          <div class="col-md-12 gris" id="neumatiocos">
              <div class="col-md-4 promocionImg">
                  <img alt="img cambio aceite" src="img/ruedas_nuevas.jpg">
              </div>
                <div class="col-md-8 captionOferta">
                  <h3 class="colorfont_amarillo center">
                    SUPER OFERTA DE NEUMÁTICOS
                  </h3>
                  <p class="colorfont_blanco p400">
                    Aprovecha la mejor oferta que encontraras en neumáticos. Informate sobre nuestra promoción en neumáticos Continental que incluye el montaje y mucho más desde 78'45 € <br><br>
                    
                    - Neumáticos 205 / 55 R16 91Y MABOR ( CONTINENTAL ). <br>
                    - La promoción incluye el  equilibrado, válvula, inflado, contrato garantía de neumático.<br>
                    - Incluye mano de obra e impuestos.<br>
                  </p>
                </div>
            </div>
          </div>

          <div class="row row_margin oferta">
            <div class="col-md-12 gris" id="camara">
              <div class="col-md-4 promocionImg">
                  <img alt="img cambio aceite" src="img/camara_trasera.jpg">
              </div>
                <div class="col-md-8 captionOferta">
                  <h3 class="colorfont_amarillo center">
                    CAMARA TRASERA
                  </h3>
                  <p class="colorfont_blanco p400">
                   ¿Estas pensando en ponerte una pantalla multimedia? No te pierdas nuestra mejor oferta que incluye de regalo una camara trasera y el montaje completo, valorado en 200€. <br><br>

                   - La promoción incluye aparato multimedia SOUND TRACK con pantalla tactil, conexiones, embellecedores, camara trasera, montaje e instalación de todo.<br>
                   - Se activa al poner la marcha atrás con el motor encendido.<br>
                   - Precio total: 495€.
                   - Incluye mano de obra e impuestos.<br>
                  </p>
                </div>
            </div>
          </div>

          <div class="row row_margin oferta">
          <div class="col-md-12 gris" id="amortiguadores">
              <div class="col-md-4 promocionImg">
                  <img alt="img cambio aceite" src="img/amortiguadores.png">
              </div>
                <div class="col-md-8 captionOferta">
                  <h3 class="colorfont_amarillo center">
                    4 X 3 AMORTIGUADORES
                  </h3>
                  <p class="colorfont_blanco p400">
                    La función de los amortiguadores es mantener la adhesión del coche a la calzada, reduciendo las oscilaciones del vehículo.<br>
                    Forman parte del triángulo de seguridad del vehículo : frenado, neumáticos, amortiguadores. Con nuestra oferta llevate 4 por el precio de 3 <br><br>

                   - La promoción incluye los 4 amortiguadores a gusto del cliente por el precio de 3.<br>
                   - Consultar disponibilidad y precios de amortiguadores.<br>
                   - Incluye mano de obra e impuestos.<br>
                  </p>
                </div>
            </div>
            </div>
            <div class="row row_margin oferta">
            <div class="col-md-12 gris" id="alfombrillas">
              <div class="col-md-4 promocionImg">
                  <img alt="img cambio aceite" src="img/alfombrillas.jpg">
              </div>
                <div class="col-md-8 captionOferta">
                  <h3 class="colorfont_amarillo center">
                    ALFOMBRILLAS A MEDIDA 
                  </h3>
                  <p class="colorfont_blanco p400">
                   Ven y llevate el pack de alfombrillas a medida para tu vehiculo desde 19'95€.  <br><br>

                   - La promoción incluye pack de alfombrillas a medida de las mejores marcas.<br>
                   - Consultar precios segun textura, modelo y marca.<br>
                  </p>
                </div>
            </div>
          </div>

           <div class="row row_margin oferta">
            <div class="col-md-12 gris" id="cascos">
              <div class="col-md-4 promocionImg">
                  <img alt="img cambio aceite" src="img/cascos.png">
              </div>
                <div class="col-md-8 captionOferta">
                  <h3 class="colorfont_amarillo center">
                    CASCOS DE MOTO
                  </h3>
                  <p class="colorfont_blanco p400">
                  Disfruta de nuestra seccion de cascos de moto en nuestra tienda, tenemos de todos los tipos, colores y medidas. <br><br>

                   - La promoción incluye casco una de las marcas lideres en seguridad.<br>
                   - Consultar precios segun tipo y extras.<br>
                   - Tipos: Clásicos, jet, jet con visera, integral, de competición, diseño.
                  </p>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
 </main>
   <div Id="clearBoth"></div>
    <?php
     require_once 'code/footer.php';
     ?>
  </div>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/validate.min.js"></script>
    <?php include_once("code/analyticstracking.php") ?>
  </body>
</html>